<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Equipment;
use App\Testimonial;

Route::get('/wallpapers','Frontend\PageController@wallpapers');

Route::get('/',function(){
    return view('frontend.home');
});
// Route::get('/', 'Frontend\PageController@home')->name('home');

Route::post('newsletter/subscribe','Frontend\PageController@NewsLetter')->name('newsletter.subscribe');
Route::get('/category/{slug}/{sub_slug}','Frontend\PageController@category');
Route::get('/category/{slug}','Frontend\PageController@category');

Route::get('/products/{slug}','Frontend\PageController@products');

Route::get('/search','Frontend\PageController@Search');

Route::get('/contact',function(){
    return view('frontend.contact');
});
Route::get('/about',function(){
    $testimonials= Testimonial::all();
    return view('frontend.about')->with('testimonials',$testimonials);
});
Route::get('/order/{slug}','Frontend\PageController@Order');

Route::get('/services',function(){
    return view('frontend.services');
});

Route::post('post/message','Frontend\PageController@Contact')->name('contact.us');
Route::post('post/order','Frontend\PageController@GeneralOrder')->name('general.order');


// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/test', 'HomeController@index');

Route::post('/subscribe','HomeController@subscribe')->name('subscribe');
Route::get('unsubscribe/{token}','HomeController@unsubscribe')->name('unsubscribe');
Route::post('unsubscribe/{token}','HomeController@unsubscribed')->name('unsubscribed');



Route::prefix('admin')->group(function(){
    Route::get('/login','Admin\Auth\LoginController@showLoginForm')->name('admin_login');
	Route::post('/login','Admin\Auth\LoginController@login');
	Route::post('/logout','Admin\Auth\LoginController@logout')->name('admin_logout');
	Route::post('/password/email','Admin\Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin_password.email');
	Route::post('/password/reset','Admin\Auth\ResetPasswordController@reset');
	Route::get('/password/reset','Admin\Auth\ForgotPasswordController@showLinkRequestForm')->name('admin_password.request');
	Route::get('/password/reset/{token}','Admin\Auth\ResetPasswordController@showResetForm')->name('admin_password.reset');
    
    Route::get('profile','Admin\ProfileController@index')->name('profile');
    Route::post('profile/changePassword','Admin\ProfileController@changePassword')->name('profile.changePassword');
    Route::post('profile/changeName','Admin\ProfileController@updateName')->name('profile.updateName');
    
    Route::get('manage/categories','Admin\CategoryController@index');

    Route::post('albums/{id}/addImages','Admin\AlbumController@addImages')->name('albums.addImages');
    Route::delete('albums/{album_id}/deleteImage/{image_id}','Admin\AlbumController@deleteImage')->name('albums.deleteImage');
    Route::resource('albums', 'Admin\AlbumController',['except' => ['create','edit']]);

    Route::get('settings','Admin\SettingsController@index')->name('admin.settings');
    Route::post('settings/addImage','Admin\SettingsController@addImage')->name('admin.settings.addImage');
    Route::delete('settings/removeImage/{id}','Admin\SettingsController@removeImage')->name('admin.settings.removeImage');
    Route::post('settings/contactUpdate','Admin\SettingsController@contactUpdate')->name('admin.settings.contactUpdate');
    Route::post('settings/aboutUpdate','Admin\SettingsController@aboutUpdate')->name('admin.settings.aboutUpdate');
    
    Route::get('newsletter','Admin\NewsletterController@newsletter')->name('newsletter');
    Route::post('newsletter/send','Admin\NewsletterController@newsletterSend')->name('newsletterSend');

    Route::get('contact-us-messages/unseen','Admin\ContactUsMessageController@unseenMsg')->name('contact-us-messages.unseen');    
    Route::get('contact-us-messages/reply/{id}','Admin\ContactUsMessageController@reply')->name('contact-us-messages.reply');
    Route::post('contact-us-messages/reply/{id}','Admin\ContactUsMessageController@replySend')->name('contact-us-messages.replySend');
    Route::resource('contact-us-messages', 'Admin\ContactUsMessageController',['only' => ['show','destroy','index']]);
    Route::get('getUnseenMsgCount','Admin\ContactUsMessageController@getUnseenMsgCount')->name('getUnseenMsgCount');
    Route::get('getUnseenMsg','Admin\ContactUsMessageController@getUnseenMsg')->name('getUnseenMsg');
    
    //order Routes
    Route::get('orders/all','Admin\OrderController@allOrders')->name('all.orders');
    Route::post('orders/delete','Admin\AdminController@DeleteOrder')->name('delete.order');
    Route::get('orders/{id}','Admin\AdminController@ShowOrder')->name('show.order');
    Route::get('orders','Admin\OrderController@index')->name('orders.index');
    Route::get('getUnseenOrderCount','Admin\OrderController@getUnseenOrderCount')->name('getUnseenOrderCount');
    Route::get('getUnseenOrder','Admin\OrderController@getUnseenOrder')->name('getUnseenOrder');
    Route::post('markseen/order','Admin\OrderController@MarkSeen');

    Route::resource('testimonials', 'Admin\TestimonialController',['except' => ['create','show']]);

    Route::get('dashboard','Admin\AdminController@index')->name('admin_dashboard');
    Route::post('test','Admin\AdminController@test')->name('test');

    Route::get('/','Admin\AdminController@index');
    
    //Category routes
    Route::post('/manage/categories','Admin\CategoryController@StoreCat')->name('category.add');
    Route::post('/edit/category','Admin\CategoryController@EditCat')->name('category.edit');
    Route::post('/delete/category','Admin\CategoryController@DestroyCat')->name('category.delete');
    
    //subcategory routes
    Route::post('/manage/subcategories','Admin\CategoryController@StoreSubCat')->name('subcategory.add');
    Route::post('/delete/subcategory','Admin\CategoryController@DestroySubCat')->name('subcategory.delete');
    Route::post('/edit/subcategory','Admin\CategoryController@EditSubCat')->name('subcategory.edit');
    
    //Product routes
    Route::get('/manage/products','Admin\ProductController@index');
    Route::get('/add/product','Admin\ProductController@viewform');
    Route::get('/product-subcat/{id}','Admin\ProductController@getSubCat');
    Route::post('/store/product','Admin\ProductController@StoreProduct')->name('product.store');
    Route::get('/filter/products/{subcat_id}','Admin\ProductController@FilterProduct');
    Route::post('delete/product','Admin\ProductController@DeleteProduct')->name('product.delete');
    Route::post('/product/feature','Admin\ProductController@FeatureProduct');
    Route::post('/product/activate','Admin\ProductController@ActivateProduct');
    Route::get('/product/edit/{id}','Admin\ProductController@EditProduct');
    Route::post('/product/edit/{id}','Admin\ProductController@ChangeProduct')->name('product.edit');
    Route::post('product/image/delete','Admin\ProductController@RemoveProductImage')->name('productimage.delete');
    
    //Equipment Routes
    Route::get('equipments','Admin\EquipmentController@index')->name('equipment.index');
    Route::post('equipments/store','Admin\EquipmentController@store')->name('equipment.store');
    Route::post('equipments/edit','Admin\EquipmentController@edit')->name('equipment.edit');
    Route::post('equipments/delete','Admin\EquipmentController@delete')->name('equipment.delete');

});

Route::get('/asset/{source}/{img}/{h}/{w}',function($source,$img, $h, $w){
    $ext = explode(".",$img);
    $ext = end($ext);
    $source = str_replace('*','/',$source);
    return \Image::make(public_path("/".$source."/".$img))->resize($h, $w)->response($ext);
})->name('optimize');

