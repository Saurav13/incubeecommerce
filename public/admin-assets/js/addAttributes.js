angular.module('SdpProduct', [])
.controller('AttributesController', ['$scope', function($scope){
    $scope.colors = [];
    $scope.sizes =[];    
    $scope.attributes=[{name:"",value:""}];
    $scope.addColorfield=function(){
      $scope.colors.push({});
    }
    $scope.delColor = function(i){
        $scope.colors.splice(i,1);
    }
    $scope.addSize = function(){
        $scope.sizes.push({});
    }
    $scope.delSize = function(i){
        $scope.sizes.splice(i,1);
    }
    $scope.addAttributeField=function(){
      $scope.attributes.push({});
    }
    $scope.delColor = function(i){
        $scope.attributes.splice(i,1);
    }
}])
