<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Category;
use Auth;

class FrontendNavComposer
{
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $categories = Category::all();
        $view->with('categories',$categories);
    }
}