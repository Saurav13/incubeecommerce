<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Subcategory;
use App\Product;
use App\ProductImage;
use Response;
use Image;
use Session;

class ProductController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }

    public function index()
    {
        $subcategories=Subcategory::all();
        $categories=Category::all();
        $products=Product::all();
        $images=ProductImage::all();
        return view('admin.product.index')->with('categories',$categories)->with('subcategories',$subcategories)->with('products',$products)->with('images',$images);
    }

    public function viewform()
    {
        $subcategories=Subcategory::all();
        $categories=Category::all();
        return view('admin.product.addproduct')->with('categories',$categories)->with('subcategories',$subcategories);
    }

    public function getSubCat($id)
    {
        $subcategories=Subcategory::where('cat_id',$id)->get();
        return Response::json($subcategories);
    }

    public function StoreProduct(Request $request)
    {
        $this->validate($request, array(
            'product_name'=>'required|max:255',
            'product_description'=>'required',
            'product_images.*' => 'required|mimes:jpg,jpeg,png,bmp|max:20000',
            'feature'=>'required',
            'active'=>'required',
            'product_category'=>'required',
            'product_subcategory'=>'required',
        ));
        $product= new Product;
        $product->product_name=$request->product_name;
        $product->product_description=$request->product_description;
        $product->featured=$request->feature;
        $product->active=$request->active;
        $product->details=$request->attributes_product;
        $product->subcat_id=$request->product_subcategory;
         //Removed empty values from json object of sizes
         $size=json_decode($request->sizes,true);
         $size = (array) array_filter((array) $size);
         $size_result = json_encode($size);
         $product->sizes=$size_result;
         //For Colors
         $colors=json_decode($request->colors,true);
         $colors=(array) array_filter((array) $colors);
         $color_result = json_encode($colors);
         $product->colors=$color_result;
   
        $product->cat_id=$request->product_category;
        
        $product->save();
        if($request->hasFile('product_images'))
        {
            $photos = $request->file('product_images');
            foreach($photos as $file)
            {
                $product_image=new ProductImage;
                $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
                $pathsmall = public_path('product-images/180x180/'.$filename);
                $pathlarge = public_path('product-images/750x500/'.$filename);
                Image::make($file)->resize(180, 180)->save($pathsmall);
                Image::make($file)->resize(650, 750)->save($pathlarge);
                $product_image->image = $filename;
                $product_image->product_id=$product->id;
                $product_image->save();
            }
        }
        Session::flash('success', 'Product was added');
        return redirect('admin/manage/products');
    }

    public function FilterProduct($subcat_id)
    {
        $subcategories=Subcategory::all();
        $categories=Category::all();
        $products=Product::where('subcat_id','=',$subcat_id)->get();
        if(count($products) != null)
        {
            $images=collect([]);
            foreach($products as $p){
                $image=ProductImage::where('product_id','=',$p->id)->get();
                $images=$images->merge($image);   
            }
        }
        if(count($products) != null)
        {
            return view('admin.product.index')->with('categories',$categories)->with('subcategories',$subcategories)->with('products',$products)->with('images',$images);
        }
        else
        {
            return view('admin.product.index')->with('categories',$categories)->with('subcategories',$subcategories)->with('products',$products);
        }
    }

    public function DeleteProduct(Request $request)
    {
        $product=Product::findorFail($request->id);
        $images=ProductImage::where('product_id','=',$product->id)->get();
        foreach($images as $i)
        {
            unlink('product-images/180x180'.'/'.$i->image);
            unlink('product-images/750x500'.'/'.$i->image);
        }
        $product->delete();
        return redirect('admin/manage/products');
    }

    public function EditProduct($id)
    {
        $categories=Category::all();
        $subcategories=Subcategory::all();
        $product=Product::findorFail($id);
        $images=ProductImage::where('product_id','=',$id)->get();
        return view('admin.product.editproduct')->with('categories',$categories)->with('product',$product)->with('images',$images)->with('subcategories',$subcategories);
    }

    public function ChangeProduct(Request $request,$id)
    {
        $this->validate($request, array(
            'product_name'=>'required|max:255',
            'product_description'=>'required',
            'product_images.*' => 'mimes:jpg,jpeg,png,bmp|max:20000',
            'feature'=>'required',
            'active'=>'required',
            'product_category'=>'required',
            'product_subcategory'=>'required',
        ));
        $product= Product::findorFail($id);
        $product->product_name=$request->product_name;
        $product->product_description=$request->product_description;
        $product->featured=$request->feature;
        $product->active=$request->active;
        $product->subcat_id=$request->product_subcategory;
        $product->cat_id=$request->product_category;
        $product->details=$request->attributes_product;
        //Removed empty values from json object of sizes
        $size=json_decode($request->sizes,true);
        $size = (array) array_filter((array) $size);
        $size_result = json_encode($size);
        $product->sizes=$size_result;
        //For Colors
        $colors=json_decode($request->colors,true);
        $colors=(array) array_filter((array) $colors);
        $color_result = json_encode($colors);
        $product->colors=$color_result;
        $product->save();
        if($request->hasFile('product_images'))
        {
            $photos = $request->file('product_images');
            foreach($photos as $file)
            {
                $product_image=new ProductImage;
                $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
                $pathsmall = public_path('product-images/180x180/'.$filename);
                $pathlarge = public_path('product-images/750x500/'.$filename);
                Image::make($file)->resize(180, 180)->save($pathsmall);
                Image::make($file)->resize(650, 750)->save($pathlarge);
                $product_image->image = $filename;
                $product_image->product_id=$product->id;
                $product_image->save();
            }
        }
        Session::flash('success', 'Product was updated');
        return redirect('admin/manage/products');
    }

    public function RemoveProductImage(Request $request)
    {
        $image=ProductImage::findorFail($request->id);
        unlink('product-images/180x180'.'/'.$image->image);
        unlink('product-images/750x500'.'/'.$image->image);
        $image->delete();
    }

    public function FeatureProduct(Request $request)
    {
        $product=Product::findorFail($request->id);
        if($product->featured == true)
        {
            $product->featured=false;
            $product->save();
            return "unfeatured";
        }
        else{
            $product->featured=true;
            $product->save();
            return "featured";
        }
    }

    public function ActivateProduct(Request $request)
    {
        $product=Product::findorFail($request->id);
        if($product->active == true)
        {
            $product->active=false;
            $product->save();
            return "Inactive";
        }
        else{
            $product->active=true;
            $product->save();
            return "Active";
        }
    }

   


}
