<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Subcategory;
use App\Product;
use App\ProductImage;
use Session;
use App\ContactUsMessage;
use App\Order;
use App\Testimonial;
use App\Subscriber;
use App\LandingImage;

class PageController extends Controller
{
    public function home()
    {
        $featured_products=Product::where('active','=',true)->where('featured','=',true)->get();
        $images=collect([]);
        $testimonials =Testimonial::all();
        $landing_images = LandingImage::all();

        foreach($featured_products as $p){
            $image=ProductImage::where('product_id','=',$p->id)->get();
            $images=$images->merge($image);   
        }
        
        return view('frontend.landing')->with('featured_products',$featured_products)->with('images',$images)->with('testimonials',$testimonials)->with('landing_images',$landing_images);
    }

    public function category($slug)
    {
        $category=Category::where('slug',$slug)->first();
        $subcategories=Subcategory::where('cat_id',$category->id)->get();
        $products=Product::where('active','=',true)->get();
        $images=ProductImage::all();
        return view('frontend.category')->with('category',$category)->with('subcategories',$subcategories)->with('products',$products)->with('images',$images);
    }

    public function products($slug)
    {   
        $product=Product::where('active','=',true)->where('slug','=',$slug)->first();
        $images=ProductImage::where('product_id','=',$product->id)->get();
        return view('frontend.product')->with('product',$product)->with('images',$images);
    }

    public function wallpapers(){
        $products = Product::where('active','=',true)->paginate(9);
        $categories = Category::all();
        return view('frontend.wallpapers',compact('products','categories'));

    }

    public function Search(Request $request)
    {
        dd($request->categoryName);
        $query=$request->search;
        $subcat=Subcategory::all();
        $products=Product::where('active','=',true)->where('product_name', 'LIKE', "%$query%")->paginate(10);
        
        return view ('frontend.search')->with('products',$products)->with('query',$query)->with('subcat',$subcat);
    }

    public function Contact(Request $request)
    {  
        $this->validate(
            $request, 
            [   
                'email'             => 'required|email',
                'name'          => 'required|max:255',
                'number' => 'required|numeric',
                'message' => 'required|max:500',
                'subject'=>'required',
                
            ],
            [   
                'email.required'    => 'Please Provide Your Email Address.',
                'email.email'      => 'Sorry, Your email is not a valid email',
                'name.required' => 'Name is required.',
                'name.max'      => 'Your name should be less than 255 charcaters',
                'number.required' => 'Number is required',
                'number.numeric' => 'Enter a valid number.',
                'message' => 'A message is required',
            ]
        );

        $contact= new ContactUsMessage;
        $contact->name= $request->name;
        $contact->email=$request->email;
        $contact->subject=$request->subject;
        $contact->message=$request->message;
        $contact->phone=$request->number;
        $contact->seen=false;
        $contact->save();
        Session::flash('success','Your Message was sent.');
        return redirect("/");


    }
    
    public function Order($slug)
    {
        $product=Product::where('slug','=',$slug)->first();
        $image=Productimage::where('product_id','=',$product->id)->first();
        return view('frontend.order')->with('product',$product)->with('image',$image);

    }

    public function GeneralOrder(Request $request)
    {
        $this->validate(
            $request, 
            [   
                'email' => 'required|email',
                'name' => 'required|max:255',
                'number' => 'required|numeric',
                'message' => 'required|max:500',            
            ],
            [   
                'email.required'    => 'Please Provide Your Email Address.',
                'email.email'      => 'Sorry, Your email is not a valid email',
                'name.required' => 'Your name is required.',
                'name.max'      => 'Your name should be less than 255 charcaters',
                'number.required' => 'Number is required',
                'number.numeric' => 'Enter a valid number.',
            ]
        );
        $order= new Order;
        $order->name= $request->name;
        $order->email=$request->email;
        $order->number=$request->number;
        $order->message=$request->message;
        $order->quantity=$request->quantity;
        $order->product_name=$request->product_name;
        $order->seen=false;
        
        if($request->hasFile('file')){
            $file = $request->file('file');
             $filename = time().rand(111,999). '.' . $file->getClientOriginalExtension();
             $path = public_path('order-files/'.$filename);
             $order->file = $filename;
         }
        $order->save();
        Session::flash('success','Your Order was recieved. You will get back to you shortly. Thank you!');
        return redirect("/");
        
    }
    public function NewsLetter(Request $request)
    {
        $subscriber=new Subscriber;
        $subscriber->email=$request->email;
        $subscriber->token=$request->_token;
        $subscriber->save();
        Session::flash('success','Thank you for joining our newsletter!');
        return redirect("/");

    }

}
