@extends('layouts.main') 
@section('body')
@section('title','| Services')
    <style>
        .inner-list{
            display: none;
        }
        .inner-list.show{
            display: block;
        }
    </style>

    <!-- Promo Block -->
    {{-- <section class="dzsparallaxer auto-init height-is-based-on-content use-loading mode-scroll loaded dzsprx-readyall" data-options='{direction: "fromtop", animation_duration: 25, direction: "reverse"}'>
        <div class="divimage dzsparallaxer--target w-100 g-bg-pos-top-center g-bg-cover g-bg-black-opacity-0_1--after" style="height: 140%; background-image: url(frontend/assets/img-temp/1920x1080/img3.jpg);"></div>

        <div class="container g-color-white g-pt-100 g-pb-40">
            <div class="g-mb-50">
            <span class="d-block g-color-white-opacity-0_8 g-font-weight-300 g-font-size-20">Best hand-made</span>
            <h3 class="g-color-white g-font-size-50 g-font-size-90--md g-line-height-1_2 mb-0">Innovative.</h3>
            <p class="g-color-white g-font-weight-600 g-font-size-20 text-uppercase">Trends 2018</p>
            </div>

            <div class="d-flex justify-content-end">
            <ul class="u-list-inline g-bg-gray-dark-v1 g-font-weight-300 g-rounded-50 g-py-5 g-px-20">
                <li class="list-inline-item g-mr-5">
                <a class="u-link-v5 g-color-white g-color-primary--hover" href="#!">Home</a>
                <i class="g-color-white-opacity-0_5 g-ml-5">/</i>
                </li>
                <li class="list-inline-item g-mr-5">
                <a class="u-link-v5 g-color-white g-color-primary--hover" href="#!">Pages</a>
                <i class="g-color-white-opacity-0_5 g-ml-5">/</i>
                </li>
                <li class="list-inline-item g-color-primary g-font-weight-400">
                <span>Grid Filter Left Sidebar</span>
                </li>
            </ul>
            </div>
        </div>
    </section> --}}
    <!-- End Promo Block -->
    
    <!-- Products -->
    <div class="container">
        <div class="row g-pb-20">
            <!-- Content -->
            <div class="col-md-9 order-md-2">
                <div class="g-pl-15--lg">

                    <!-- Products -->
                    <div class="row g-pt-30 g-mb-50">
                        {{-- @foreach ($products as $item)
                            <div class="col-6 col-lg-4 g-mb-30">
                                <!-- Product -->
                                <figure class="g-pos-rel g-mb-20">
                                    @foreach($item->images->slice(0,1) as $i)
                                        <img class="img-fluid" src="{{asset('product-images/750x500'.'/'.$i->image)}}" alt="Image Description">
                                    @endforeach
                                    @if($item->featured)
                                        <figcaption class="w-100 g-bg-primary g-bg-black--hover text-center g-pos-abs g-bottom-0 g-transition-0_2 g-py-5">
                                            <a class="g-color-white g-font-size-11 text-uppercase g-letter-spacing-1 g-text-underline--none--hover" href="#!">Featured</a>
                                        </figcaption>
                                    @endif
                                </figure>

                                <div class="media">
                                    <!-- Product Info -->
                                    <div class="d-flex flex-column">
                                        <h4 class="h6 g-color-black mb-1">
                                            <a class="u-link-v5 g-color-black g-color-primary--hover" href="{{URL::to('products'.'/'.$item->slug)}}">
                                                {{$item->product_name}}
                                            </a>
                                        </h4>
                                        <span class="d-block g-color-black g-font-size-17">Rs.52</span>
                                    </div>
                                    <!-- End Product Info -->
        
                                    <!-- Products Icons -->
                                    <ul class="list-inline media-body text-right">
                                        <li class="list-inline-item align-middle mx-0">
                                            <a class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-color-primary--hover g-font-size-15 rounded-circle"
                                                href="{{URL::to('order'.'/'.$item->slug)}}"
                                                data-toggle="tooltip"
                                                data-placement="top"
                                                title="Place Order">
                                                <i class="icon-medical-022 u-line-icon-pro"></i>
                                            </a>
                                        </li>
                                    </ul>
                                    <!-- End Products Icons -->
                                </div>
                                <!-- End Product -->
                            </div>
                        @endforeach --}}
                        <canvas id="myCanvas" width="700" height="500" style="cursor:pointer"></canvas>
                        <br>
                        <div style="">
                            <a href="javascript:void(0)" style="cursor:pointer" onclick="changewallpaper('/bedroom/wallpaper1.png')"><img src="/bedroom/wallpaper1.png" height="100" width="100" /></a>
                            <a href="javascript:void(0)" style="cursor:pointer" onclick="changewallpaper('/bedroom/wallpaper2.png')"><img src="/bedroom/wallpaper2.png" height="100" width="100" /></a>
                            <a href="javascript:void(0)" style="cursor:pointer" onclick="changewallpaper('/bedroom/wallpaper3.jpg')"><img src="/bedroom/wallpaper3.jpg" height="100" width="100" /></a>
                            <a href="javascript:void(0)" style="cursor:pointer" onclick="changewallpaper('/bedroom/wallpaper4.jpg')"><img src="/bedroom/wallpaper4.jpg" height="100" width="100" /></a>
                            <a href="javascript:void(0)" style="cursor:pointer" onclick="changewallpaper('/bedroom/wallpaper5.jpg')"><img src="/bedroom/wallpaper5.jpg" height="100" width="100" /></a>
                            <a href="javascript:void(0)" style="cursor:pointer" onclick="changewallpaper('/bedroom/wallpaper6.jpg')"><img src="/bedroom/wallpaper6.jpg" height="100" width="100" /></a>
                        </div>
                    </div>
                    <!-- End Products -->

                    <hr class="g-mb-60">

                    <!-- Pagination -->
                    <nav class="g-mb-100" aria-label="Page Navigation">
                        {{-- {{ $products->links('frontend.partials.paginate') }} --}}
                    </nav>
                    <!-- End Pagination -->
                </div>
            </div>
            <!-- End Content -->

            <!-- Filters -->
            <div class="col-md-3 order-md-1 g-brd-right--lg g-brd-gray-light-v4 g-pt-30">
                <form class="g-pr-15--lg g-pt-30" action="/search">
                    <!-- Categories -->
                    <div class="g-mb-30">
                        <h3 class="h5 mb-3">Categories</h3>

                        <ul class="list-unstyled" id="categories" nav-parent="0">
                            @foreach ($categories as $c)
                                <li class="my-3 g-color-gray-dark-v4 inner-list show" nav-parent="0" nav-link="{{ $c->cat_name }}">

                                    <label class="u-check">
                                        <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="categoryName" type="radio" value="{{ $c->cat_name }}">
                                        <span class="u-check-icon-checkbox-v4 g-brd-none" style="font-size: 14px;">
                                                {{ $c->cat_name }}
                                        </span>
                                    </label>
                                    @if($c->subcategories->count() > 0)
                                        {{-- <a class="u-link-v5 g-color-gray-dark-v4" href="#!">{{ $c->cat_name }}</a> --}}
                                        <a href="#" class="float-right g-font-size-10" style="margin-top: 4px;"><i class="hs-icon hs-icon-arrow-right"></i>
                                        </a>
                                    @endif


                                </li>
                                @if($c->subcategories->count() > 0)
                                    <ul class="list-unstyled inner-list" nav-parent="{{ $c->cat_name }}">
                                        <li class="my-3" nav-link="0">
                                            <a class="d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover" href="#!">
                                                <i class="hs-icon hs-icon-arrow-left"></i>
                                            </a>
                                        </li>
                                        @foreach ($c->subcategories as $s)
                                            {{-- <li class="my-3" nav-link="{{ $s->subcat_name}}">
                                                <a href="#" class="d-block u-link-v5 g-color-gray-dark-v4">{{ $s->subcat_name }}
                                                    
                                                </a>
                                            </li> --}}
                                            <li class="my-3 g-color-gray-dark-v4" nav-link="{{ $s->subcat_name}}">
                                                <label class="u-check">
                                                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="categoryName" type="radio" value="{{ $s->subcat_name }}">
                                                    <span class="u-check-icon-checkbox-v4 g-brd-none" style="font-size: 14px;">
                                                            {{ $s->subcat_name }}
                                                    </span>
                                                </label>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            @endforeach
                            
                        </ul>

                    </div>
                    <!-- End Categories -->

                    <hr>

                    <!-- Pricing -->
                    <div class="g-mb-30">
                    <h3 class="h5 mb-3">Pricing</h3>

                    <div class="text-center">
                        <span class="d-block g-color-primary mb-4">Rs.(<span id="rangeSliderAmount3">0</span>)</span>
                        <div id="rangeSlider1" class="u-slider-v1-3"
                            data-result-container="rangeSliderAmount3"
                            data-range="true"
                            data-default="1000, 4000"
                            data-min="0"
                            data-max="50000"></div>
                    </div>
                    </div>
                    <!-- End Pricing -->

                    <hr>

                    <!-- Brand -->
                    {{-- <div class="g-mb-30">
                    <h3 class="h5 mb-3">Brand</h3>

                    <ul class="list-unstyled">
                        <li class="my-2">
                        <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                            <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                            <i class="fa" data-check-icon="&#xf00c"></i>
                            </span>
                            Mango <span class="float-right g-font-size-13">24</span>
                        </label>
                        </li>
                        <li class="my-2">
                        <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox" checked>
                            <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                            <i class="fa" data-check-icon="&#xf00c"></i>
                            </span>
                            Gucci <span class="float-right g-font-size-13">334</span>
                        </label>
                        </li>
                        <li class="my-2">
                        <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                            <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                            <i class="fa" data-check-icon="&#xf00c"></i>
                            </span>
                            Adidas <span class="float-right g-font-size-13">18</span>
                        </label>
                        </li>
                        <li class="my-2">
                        <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox" checked>
                            <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                            <i class="fa" data-check-icon="&#xf00c"></i>
                            </span>
                            Nike <span class="float-right g-font-size-13">6</span>
                        </label>
                        </li>
                        <li class="my-2">
                        <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                            <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                            <i class="fa" data-check-icon="&#xf00c"></i>
                            </span>
                            Puma <span class="float-right g-font-size-13">71</span>
                        </label>
                        </li>
                        <li class="my-2">
                        <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                            <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                            <i class="fa" data-check-icon="&#xf00c"></i>
                            </span>
                            Zara <span class="float-right g-font-size-13">9</span>
                        </label>
                        </li>
                    </ul>
                    </div> --}}
                    <!-- End Brand -->

                    <hr>

                    <!-- Color -->
                    <div class="g-mb-30">
                    <h3 class="h5 mb-3">Color</h3>

                    <!-- Checkbox -->
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item g-mr-10">
                        <label class="form-check-inline u-check">
                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="radInline1_1" type="radio">
                            <span class="d-block u-check-icon-checkbox-v4 g-brd-transparent g-brd-gray-dark-v4--checked rounded-circle g-absolute-centered--y g-left-0 g-mt-3">
                            <i class="d-block g-absolute-centered g-width-16 g-height-16 g-bg-primary rounded-circle"></i>
                            </span>
                        </label>
                        </li>
                        <li class="list-inline-item g-mx-10">
                        <label class="form-check-inline u-check">
                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="radInline1_1" type="radio">
                            <span class="d-block u-check-icon-checkbox-v4 g-brd-transparent g-brd-gray-dark-v4--checked rounded-circle g-absolute-centered--y g-left-0 g-mt-3">
                            <i class="d-block g-absolute-centered g-width-16 g-height-16 g-bg-beige rounded-circle"></i>
                            </span>
                        </label>
                        </li>
                        <li class="list-inline-item g-mx-10">
                        <label class="form-check-inline u-check">
                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="radInline1_1" type="radio">
                            <span class="d-block u-check-icon-checkbox-v4 g-brd-transparent g-brd-gray-dark-v4--checked rounded-circle g-absolute-centered--y g-left-0 g-mt-3">
                            <i class="d-block g-absolute-centered g-width-16 g-height-16 g-bg-black rounded-circle"></i>
                            </span>
                        </label>
                        </li>
                        <li class="list-inline-item g-mx-10">
                        <label class="form-check-inline u-check">
                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="radInline1_1" type="radio">
                            <span class="d-block u-check-icon-checkbox-v4 g-brd-transparent g-brd-gray-dark-v4--checked rounded-circle g-absolute-centered--y g-left-0 g-mt-3">
                            <i class="d-block g-absolute-centered g-width-16 g-height-16 g-bg-yellow rounded-circle"></i>
                            </span>
                        </label>
                        </li>
                        <li class="list-inline-item g-mx-10">
                        <label class="form-check-inline u-check">
                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="radInline1_1" type="radio">
                            <span class="d-block u-check-icon-checkbox-v4 g-brd-transparent g-brd-gray-dark-v4--checked rounded-circle g-absolute-centered--y g-left-0 g-mt-3">
                            <i class="d-block g-absolute-centered g-width-16 g-height-16 g-bg-blue rounded-circle"></i>
                            </span>
                        </label>
                        </li>
                        <li class="list-inline-item g-mx-10">
                        <label class="form-check-inline u-check">
                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="radInline1_1" type="radio">
                            <span class="d-block u-check-icon-checkbox-v4 g-brd-transparent g-brd-gray-dark-v4--checked rounded-circle g-absolute-centered--y g-left-0 g-mt-3">
                            <i class="d-block g-absolute-centered g-width-16 g-height-16 g-bg-purple rounded-circle"></i>
                            </span>
                        </label>
                        </li>
                        <li class="list-inline-item g-mx-10">
                        <label class="form-check-inline u-check">
                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="radInline1_1" type="radio">
                            <span class="d-block u-check-icon-checkbox-v4 g-brd-transparent g-brd-gray-dark-v4--checked rounded-circle g-absolute-centered--y g-left-0 g-mt-3">
                            <i class="d-block g-absolute-centered g-width-16 g-height-16 g-bg-brown rounded-circle"></i>
                            </span>
                        </label>
                        </li>
                        <li class="list-inline-item g-ml-10">
                        <label class="form-check-inline u-check">
                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="radInline1_1" type="radio">
                            <span class="d-block u-check-icon-checkbox-v4 g-brd-transparent g-brd-gray-dark-v4--checked rounded-circle g-absolute-centered--y g-left-0 g-mt-3">
                            <i class="d-block g-absolute-centered g-width-16 g-height-16 g-bg-gray-dark-v4 rounded-circle"></i>
                            </span>
                        </label>
                        </li>
                    </ul>
                    <!-- End Checkbox -->
                    </div>
                    <!-- End Color -->

                    <hr>

                    <button class="btn btn-block u-btn-black g-font-size-12 text-uppercase g-py-12 g-px-25" type="submit" id="FilterProduct">Filter</button>
                </form>
            </div>
            <!-- End Filters -->
        </div>
    </div>
    <!-- End Products -->
@endsection

@section('js')
<script src="/frontend/assets/vendor/jquery-ui/ui/widget.js"></script>
<script src="/frontend/assets/vendor/jquery-ui/ui/widgets/menu.js"></script>
<script src="/frontend/assets/vendor/jquery-ui/ui/widgets/mouse.js"></script>
<script src="/frontend/assets/vendor/jquery-ui/ui/widgets/slider.js"></script>
<script src="/frontend/assets/js/components/hs.slider.js"></script>
<script>
    // initialization of range slider
    $.HSCore.components.HSSlider.init('#rangeSlider1');

    $(document).ready(function(){

        $('#categories').on('click','a',function(){
            var ele = $(this);
            
            var cat = ele.parent().attr('nav-link');
            $('.inner-list').each(function(){
                $(this).removeClass('show');
            });

            $(".inner-list[nav-parent='"+cat+"']").each(function(){
                $(this).addClass('show');
            });
        });
    })
</script>
<script>
    function loadImages(sources, callback) {
      var images = {};
      var loadedImages = 0;
      var numImages = 0;
      // get num of sources
      for(var src in sources) {
        numImages++;
      }
      for(var src in sources) {
        images[src] = new Image();
        images[src].onload = function() {
          if(++loadedImages >= numImages) {
            callback(images);
          }
        };
        images[src].src = sources[src].src;
      }
    }
    var canvas = document.getElementById('myCanvas');
    var context = canvas.getContext('2d');
    // context.globalCompositeOperation = 'multiply';

    var sources = {
      mainwall: {
        src: '/bedroom/mainwall.png',
        selectable: true,
        x: 152,
        y: 120, 
        w: 371,
        h: 179
      },
      leftwall:{
        src: '/bedroom/leftwall.png',
        selectable: true,
        x: 36,
        y: 55, 
        w: 118,
        h: 291
      },
      rightwall:{
        src: '/bedroom/rightwall.png',
        selectable: true,
        x: 517,
        y: 55, 
        w: 118,
        h: 288
      },
      ceiling: {
        src: '/bedroom/ceiling.png',
        selectable: false,
        x: 35,
        y: 0, 
        w: 600,
        h: 141
      },
      floor: {
        src: '/bedroom/floor.png',
        selectable: false,
        x: 35,
        y: 287, 
        w: 600,
        h: 120
      },
      windowleft: {
        src:'/bedroom/windowleft.png',
        selectable: true,
        x: 81,
        y: 82, 
        w: 63,
        h: 207
      },
      windowright: {
        src: '/bedroom/windowright.png',
        selectable: true,
        x: 527,
        y: 81, 
        w: 65,
        h: 209
      },
      bed: {
        src:'/bedroom/bed2.png',
        selectable: false,
        x: 235,
        y: 237, 
        w: 150,
        h: 102
      }
    };

    var selected_wall,selected_wallpaper;

    loadImages(sources, function(images) {
      context.drawImage(images.mainwall, sources.mainwall.x, sources.mainwall.y, sources.mainwall.w, sources.mainwall.h);
      context.drawImage(images.leftwall, sources.leftwall.x, sources.leftwall.y, sources.leftwall.w, sources.leftwall.h);
      context.drawImage(images.rightwall, sources.rightwall.x, sources.rightwall.y, sources.rightwall.w, sources.rightwall.h);
      context.drawImage(images.windowleft, sources.windowleft.x, sources.windowleft.y, sources.windowleft.w, sources.windowleft.h);
      context.drawImage(images.windowright, sources.windowright.x, sources.windowright.y, sources.windowright.w, sources.windowright.h);
      context.drawImage(images.ceiling, sources.ceiling.x, sources.ceiling.y, sources.ceiling.w, sources.ceiling.h);
      context.drawImage(images.floor, sources.floor.x, sources.floor.y, sources.floor.w, sources.floor.h);
      context.drawImage(images.bed, sources.bed.x, sources.bed.y, sources.bed.w, sources.bed.h);
    });

    // context.scale(.5,.5)
    function changewallpaper(id){
      if(!selected_wall){
        alert('select a wall')
        return;
      }
      if(!id){
        alert('select a wallpaper')
        return;
      }
      context.clearRect(0, 0, canvas.width, canvas.height);
      
      for(var src in sources) {
        if(src == selected_wall){
          sources[src].src = id;
          break;
        }
      }

      loadImages(sources, function(images) {
        context.drawImage(images.mainwall, sources.mainwall.x, sources.mainwall.y, sources.mainwall.w, sources.mainwall.h);
        context.drawImage(images.leftwall, sources.leftwall.x, sources.leftwall.y, sources.leftwall.w, sources.leftwall.h);
        context.drawImage(images.rightwall, sources.rightwall.x, sources.rightwall.y, sources.rightwall.w, sources.rightwall.h);
        context.drawImage(images.windowleft, sources.windowleft.x, sources.windowleft.y, sources.windowleft.w, sources.windowleft.h);
        context.drawImage(images.windowright, sources.windowright.x, sources.windowright.y, sources.windowright.w, sources.windowright.h);
        context.drawImage(images.ceiling, sources.ceiling.x, sources.ceiling.y, sources.ceiling.w, sources.ceiling.h);
        context.drawImage(images.floor, sources.floor.x, sources.floor.y, sources.floor.w, sources.floor.h);
        context.drawImage(images.bed, sources.bed.x, sources.bed.y, sources.bed.w, sources.bed.h);
      });
    }

    function getMousePos(canvas, evt) {
      var rect = canvas.getBoundingClientRect();
      return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
      };
    }

    canvas.addEventListener('click', function(evt) {
      var mousePos = getMousePos(canvas, evt);
      for(var src in sources) {
        if((mousePos.x >= sources[src].x && mousePos.x<= (sources[src].x+sources[src].w)) && (mousePos.y >= sources[src].y && mousePos.y<= (sources[src].y+sources[src].h))){
          if(sources[src].selectable){
            selected_wall = src;
            document.getElementById("myCanvas").style.cursor = "pointer";
          }
          else{
            alert('Cant be selected')
            document.getElementById("myCanvas").style.cursor = "default";
          }
          break;
        }
      }
      
    }, false);
    
  </script>
@endsection