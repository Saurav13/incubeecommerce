@extends('layouts.main')
@section('title', '| Contact Us')
@section('body')
 <!-- Breadcrumbs -->
 <section class="g-bg-size-cover g-bg-pos-center g-bg-cover g-bg-black-opacity-0_5--after g-color-white g-py-50 g-mb-20" style="background-image: url({{asset('frontend/assets/img-temp/stock/cover.png')}});">
    <div class="container g-bg-cover__inner">
      <header class="g-mb-20">
        <h2 class="h1 g-font-weight-300 text-uppercase">Contact
          <span class="g-color-primary">Us</span>
        </h2>
      </header>
      <ul class="u-list-inline">
        <li class="list-inline-item g-mr-7">
          <a class="u-link-v5 g-color-white g-color-primary--hover" href="#!">Home</a>
          <i class="fa fa-angle-right g-ml-7"></i>
        </li>
        <li class="list-inline-item g-color-primary">
          <span>Contact</span>
        </li>
      </ul>
    </div>
  </section>
  <!-- End Breadcrumbs -->

  <!-- Contact Form -->
  <section class="container g-py-5">
    <div class="row g-mb-20">
      <div class="col-lg-6 g-mb-50">
        <!-- Heading -->
        <h2 class="h1 g-color-black g-font-weight-700 mb-4">How can we assist you?</h2>
        <p class="g-font-size-18 mb-0">We are a creative studio focusing on culture, luxury, editorial &amp; art. Somewhere between sophistication and simplicity.</p>
        <!-- End Heading -->
      </div>
      <div class="col-lg-3 align-self-end ml-auto g-mb-50">
        <div class="media">
          <div class="d-flex align-self-center">
            <span class="u-icon-v2 u-icon-size--sm g-color-white g-bg-primary rounded-circle mr-3">
                <i class="g-font-size-16 icon-communication-033 u-line-icon-pro"></i>
              </span>
          </div>
          <div class="media-body align-self-center">
            <h3 class="h6 g-color-black g-font-weight-700 text-uppercase mb-0">Phone Number</h3>
            <p class="mb-0">+977 984565286</p>
          </div>
        </div>
      </div>

      <div class="col-lg-3 align-self-end ml-auto g-mb-50">
        <div class="media">
          <div class="d-flex align-self-center">
            <span class="u-icon-v2 u-icon-size--sm g-color-white g-bg-primary rounded-circle mr-3">
                <i class="g-font-size-16 icon-communication-062 u-line-icon-pro"></i>
              </span>
          </div>
          <div class="media-body align-self-center">
            <h3 class="h6 g-color-black g-font-weight-700 text-uppercase mb-0">Email Address</h3>
            <p class="mb-0">info@shrestha.com</p>
          </div>
        </div>
      </div>
    </div>

    <div class="row justify-content-center">
      <div class="col-md-5">
      <form action="{{route('contact.us')}}" method="POST">
         
            <div class="g-mb-20">
              <label class="g-color-gray-dark-v2 g-font-size-13">Name *</label>
            <input name="name" value="{{old('name')}}" class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus rounded-3 g-py-13 g-px-15" type="text" placeholder="John" required>
            </div>

         

          <div class="g-mb-20">
            <label class="g-color-gray-dark-v2 g-font-size-13">Your email *</label>
          <input name="email" value="{{old('email')}}" class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus rounded-3 g-py-13 g-px-15" type="email" placeholder="johndoe@gmail.com" required>
          </div>

          <div class="g-mb-20">
            <label class="g-color-gray-dark-v2 g-font-size-13">Phone number *</label>
          <input name="number" value="{{old('number')}}" class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus rounded-3 g-py-13 g-px-15" type="tel" placeholder="+ (01) 222 33 44" required>
          </div>

        </div>
        <div class="col-md-7">
            <div class="g-mb-20">
                <label class="g-color-gray-dark-v2 g-font-size-13">Subject *</label>
              <input name="subject" value="{{old('subject')}}" class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus rounded-3 g-py-13 g-px-15" type="text" placeholder="Mug printing" required>
              </div>
          <div class="g-mb-40">
          <label class="g-color-gray-dark-v2 g-font-size-13">Your message *</label>
          <textarea name="message" class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus g-resize-none rounded-3 g-py-13 g-px-15" rows="8" placeholder="Hi there, I would like to ..." required>{{old('message')}}</textarea>
        </div>
        {{csrf_field()}}
        <div class="text-right">
          <button class="btn u-btn-primary g-font-weight-600 g-font-size-13 text-uppercase rounded-3 g-py-12 g-px-35" type="submit" role="button">Send</button>
        </div>
      </div>
    </form>
    </div>
  </section>
  <!-- End Contact Form -->
  @endsection