@extends('layouts.main')
@section('body')
@section('title','| Search "'.$query.'"')

<section class="g-pt-50 g-pb-90">
    <div class="container">
      <!-- Search Info -->
      <div class="d-md-flex justify-content-between g-mb-30">
      @if(count($products) != null)
      {{-- <h3 class="h6 text-uppercase g-mb-10 g-mb--lg">About <span class="g-color-gray-dark-v1">"{{ count($products) }}"</span> results</h3> --}}
      @else
      <h3 class="h6 text-uppercase g-mb-10 g-mb--lg">No results found</h3>
      @endif
      </div>
      <!-- End Search Info -->
      <div class="row">
        @foreach($products as $p)
          <div class="col-sm-6 col-md-4 g-mb-30">
            <!-- Article -->
            <article class="g-pos-rel g-rounded-4 g-brd-bottom g-brd-3 g-brd-gray-light-v4 g-brd-primary--hover text-center g-transition-0_3 g-transition--linear">
                @foreach($p->images->slice(0,1) as $img)
              <!-- Article Image -->
              <figure>
                <img class="w-100" src="{{asset('product-images/750x500'.'/'.$img->image)}}" alt="{{$p->product_name}}"> 
                <!-- Image Caption -->
                <?php
                  $subcat_name=$subcat->where('id','=',$p->subcat_id)->first();
                ?>
              <figcaption class="u-ribbon-v1 g-color-white g-bg-purple-opacity-0_7 g-font-weight-700 g-font-size-11 text-uppercase g-top-10 g-left-10 rounded">{{$subcat_name->subcat_name}}</figcaption>
                <!-- End Image Caption -->
              </figure>
              <!-- End Article Image -->
              @endforeach
        
              <!-- Article Content -->
              <div class="g-bg-secondary g-pa-30">
                <h3 class="h4">
                <a class="g-color-main g-color-primary--hover g-text-underline--none--hover" href="{{URL::to('products'.'/'.$p->slug)}}">{{$p->product_name}}</a>
                </h3>
              </div>
              <!-- End Article Content -->
            </article>
            <!-- End Article -->
          </div>
          @endforeach
      </div>

      <hr class="g-brd-gray-light-v4 g-mt-10 g-mb-40">

      <!-- Pagination -->
      <nav class="g-mb-50" aria-label="Page Navigation" style="text-align: center;">
        {{ $products->links('frontend.partials.paginate') }}
      </nav>
      <!-- End Pagination -->
    </div>
  </section>


@endsection