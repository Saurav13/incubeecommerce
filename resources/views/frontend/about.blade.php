@extends('layouts.main')
@section('body')
@section('title','| About Us')
<div class="container">
<div class="row g-py-20">
  <div class="col-lg-7 g-mb-50 g-mb-0--lg">
    <header class="u-heading-v2-3--bottom g-brd-primary g-mb-20">
      <h2 class="h3 u-heading-v2__title text-uppercase g-font-weight-300 mb-0">Shrestha Digital Printers</h2>
    </header>

    <p class="lead g-mb-30">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas moles</p>

    <div class="row">
      <div class="col-sm-6">
        <ul class="list-unstyled g-color-gray-dark-v4 g-mb-30 g-mb-0--sm">
          <li class="d-flex g-mb-10">
            <i class="icon-check g-color-primary g-mt-5 g-mr-10"></i>
            Best Digital Printing All over Nepal
          </li>
          <li class="d-flex g-mb-10">
            <i class="icon-check g-color-primary g-mt-5 g-mr-10"></i>
            More than 200+ clients served
          </li>
          <li class="d-flex g-mb-10">
            <i class="icon-check g-color-primary g-mt-5 g-mr-10"></i>
            More services than you can imagine
          </li>
          <li class="d-flex g-mb-10">
            <i class="icon-check g-color-primary g-mt-5 g-mr-10"></i>
            Ready to go service
          </li>
        </ul>
      </div>

      <div class="col-sm-6">
        <ul class="list-unstyled g-color-gray-dark-v4">
          <li class="d-flex g-mb-10">
            <i class="icon-check g-color-primary g-mt-5 g-mr-10"></i>
            Timely Delivery
          </li>
          <li class="d-flex g-mb-10">
            <i class="icon-check g-color-primary g-mt-5 g-mr-10"></i>
            First Online Digital Printing
          </li>
          <li class="d-flex g-mb-10">
            <i class="icon-check g-color-primary g-mt-5 g-mr-10"></i>
            Time Saver
          </li>
          <li class="d-flex g-mb-10">
            <i class="icon-check g-color-primary g-mt-5 g-mr-10"></i>
            Cost Efficient
          </li>
        </ul>
      </div>
    </div>
  </div>

  <div class="col-lg-5 align-self-end">
    <img class="img-fluid" src="{{asset('frontend/assets/img-temp/stock/sdp.png')}}" style="height:270px;" alt="Image Description">
  </div>
</div>
</div>

<!-- Icon Blocks -->
<div class="clearfix">
  <div class="row no-gutters">
    <div class="col-lg-4">
      <!-- Icon Blocks -->
      <div class="u-block-hover g-bg-purple g-color-white text-center g-py-120 g-px-50">
        <span class="d-block g-font-size-50 g-rounded-5 mb-2">
            <i class="icon-education-087 u-line-icon-pro"></i>
          </span>
        <h3 class="h3 g-font-weight-600 mb-30">Extensive documentation</h3>
        <p class="lead g-color-white-opacity-0_8 mb-5">We strive to embrace and drive change in our industry which allows us to keep our clients relevant.</p>

        <div class="u-block-hover__additional--fade u-block-hover__additional--fade-up">
          <a class="btn btn-lg u-btn-outline-white g-pos-abs g-bottom-80 g-absolute-centered--x g-font-weight-500 g-font-size-default g-rounded-4 g-px-25" href="#!">Subscribe</a>
        </div>
      </div>
      <!-- End Icon Blocks -->
    </div>

    <div class="col-lg-4">
      <!-- Icon Blocks -->
      <div class="u-block-hover g-bg-teal g-color-white text-center g-py-120 g-px-50">
        <span class="d-block g-font-size-50 g-rounded-5 mb-2">
            <i class="icon-education-035 u-line-icon-pro"></i>
          </span>
        <h3 class="h3 g-font-weight-600 mb-30">Modern design</h3>
        <p class="lead g-color-white-opacity-0_8 mb-5">We strive to embrace and drive change in our industry which allows us to keep our clients relevant.</p>
        <div class="u-block-hover__additional--fade u-block-hover__additional--fade-up">
          <a class="btn btn-lg u-btn-outline-white g-pos-abs g-bottom-80 g-absolute-centered--x g-font-weight-500 g-font-size-default g-rounded-4 g-px-25" href="#!">Subscribe</a>
        </div>
      </div>
      <!-- End Icon Blocks -->
    </div>

    <div class="col-lg-4">
      <!-- Icon Blocks -->
      <div class="u-block-hover g-bg-purple g-color-white text-center g-py-120 g-px-50">
        <span class="d-block g-font-size-50 g-rounded-5 mb-2">
            <i class="icon-education-141 u-line-icon-pro"></i>
          </span>
        <h3 class="h3 g-font-weight-600 mb-30">Creative ideas</h3>
        <p class="lead g-color-white-opacity-0_8 mb-5">We strive to embrace and drive change in our industry which allows us to keep our clients relevant.</p>
        <div class="u-block-hover__additional--fade u-block-hover__additional--fade-up">
          <a class="btn btn-lg u-btn-outline-white g-pos-abs g-bottom-80 g-absolute-centered--x g-font-weight-500 g-font-size-default g-rounded-4 g-px-25" href="#!">Subscribe</a>
        </div>
      </div>
      <!-- End Icon Blocks -->
    </div>
  </div>
</div>
<!-- End Icon Blocks -->

<!-- Team -->
<div class="container g-pt-40 g-pb-70">
  <div class="text-center g-mb-50">
    <h2 class="h1 g-font-weight-300">What people say</h2>
  </div>
  
  <div class="js-carousel" data-infinite="true" data-arrows-classes="u-arrow-v1 g-width-50 g-height-50 g-brd-1 g-brd-style-solid g-brd-gray-light-v1 g-brd-primary--hover g-color-gray-dark-v5 g-bg-primary--hover g-color-white--hover g-absolute-centered--y rounded-circle g-mt-minus-25"
  data-arrow-left-classes="fa fa-angle-left g-left-0" data-arrow-right-classes="fa fa-angle-right g-right-0">
  @foreach($testimonials as $testi)
    <div class="js-slide">
      <div class="row justify-content-center">
        <div class="col-lg-7">
          <!-- Testimonial Advanced -->
          <div class="g-pos-rel g-pt-20">
            <em class="g-color-gray-light-v5 g-font-size-120 g-pos-abs g-top-minus-15 g-left-minus-15 g-z-index-minus-1">
                &#8220;
              </em>
            <blockquote class="lead g-font-style-italic g-line-height-2 g-pl-30 g-mb-30">{{$testi->content}}</blockquote>

            <div class="media">
            <img class="d-flex align-self-center g-width-50 g-height-50 rounded-circle mr-3" src="{{asset('landing_images'.'/'.$testi->photo)}}" alt="{{$testi->photo}}">

              <div class="media-body align-self-center">
              <h4 class="h5 g-font-weight-700 g-mb-0">{{$testi->name}}</h4>
              <span class="d-block g-color-gray-dark-v4">{{$testi->profession}}</span>
              </div>
            </div>
          </div>
          <!-- End Testimonial Advanced -->
        </div>
      </div>
    </div>
    @endforeach
  </div>
  
</div>
<!-- End Team -->



<div class="container text-center g-pb-100">
  <h2 class="h1 g-font-weight-300 mb-3">Join <span class="g-color-primary">SDP</span> Team</h2>
  <p class="g-color-gray g-font-weight-500 text-uppercase g-letter-spacing-1 g-mb-50">Sign up to our newsletter for latest news</p>
  <div class="row justify-content-center">
    <div class="col-lg-6">
      <form action="{{route('newsletter.subscribe')}}" method="POST" role="form">
        {{csrf_field()}}
        <div class="input-group">
          <input name="email" class="form-control g-font-size-default g-rounded-4 mr-4" type="text" placeholder="Your email">
          <div class="input-group-btn">
            <button class="btn btn-xl u-btn-primary g-font-weight-500 g-font-size-default g-rounded-4 g-px-35" type="submit">Subscribe</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection