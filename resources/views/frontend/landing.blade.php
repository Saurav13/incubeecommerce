@extends('layouts.main')
@section('css')
   <!-- Revolution Slider -->
   <link rel="stylesheet" href="{{asset('frontend/assets/vendor/revolution-slider/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}">
   <link rel="stylesheet" href="{{asset('frontend/assets/vendor/revolution-slider/revolution/css/settings.css') }}">
   <link rel="stylesheet" href="{{asset('frontend/assets/vendor/revolution-slider/revolution/css/layers.css') }}">
   <link rel="stylesheet" href="{{asset('frontend/assets/vendor/revolution-slider/revolution/css/navigation.css') }}">
   <link rel="stylesheet" href="{{asset('frontend/assets/vendor/revolution-slider/revolution-addons/typewriter/css/typewriter.css') }}">

@endsection

@section('body')

<!-- Revolution Slider -->
<div class="g-overflow-hidden">
  <div id="rev_slider_1014_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="typewriter-effect" data-source="gallery" style="background-color:transparent;padding:0px;">
    <!-- START REVOLUTION SLIDER 5.4.1 fullscreen mode -->
    <div id="rev_slider_1014_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.1">
      <ul>
        @foreach ($landing_images as $img)
          <!-- SLIDE  -->
          <li data-index="rs-2800" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="/frontend/assets/img-temp/img2.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
            <!-- MAIN IMAGE -->
            <img src="/landing_images/{{ $img->image }}" alt="" data-bgposition="center center" data-bgfit="cover" data-bgparallax="10" class="rev-slidebg">
            <!-- LAYERS -->
            <!-- LAYER NR. 1 -->
            <div class="tp-caption tp-shape tp-shapewrapper"
                id="slide-2800-layer-7"
                data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                data-width="full"
                data-height="full"
                data-whitespace="nowrap"

                data-type="shape"
                data-basealign="slide"
                data-responsive_offset="off"
                data-responsive="off"
                data-frames='[{"from":"opacity:0;","speed":500,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},{"speed":5000,"to":"opacity:0;","ease":"Power4.easeInOut"}]'
                data-textAlign="['left','left','left','left']"
                data-paddingtop="[0,0,0,0]"
                data-paddingright="[0,0,0,0]"
                data-paddingbottom="[0,0,0,0]"
                data-paddingleft="[0,0,0,0]"

                style="z-index: 5;background-color:rgba(0, 0, 0, 0.50);border-color:rgba(0, 0, 0, 0);border-width:0px;"></div>

            <!-- LAYER NR. 2 -->
            {{-- <div class="tp-caption   tp-resizeme"
                id="slide-2800-layer-1"
                data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                data-y="['middle','middle','middle','middle']" data-voffset="['-160','-160','-130','-157']"
                data-fontsize="['110','110','100','60']"
                data-lineheight="['110','110','100','60']"
                data-width="none"
                data-height="none"
                data-whitespace="nowrap"

                data-type="text"
                data-responsive_offset="on"

                data-frames='[{"from":"y:50px;sX:1;sY:1;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                data-textAlign="['center','center','center','center']"
                data-paddingtop="[0,0,0,0]"
                data-paddingright="[0,0,0,0]"
                data-paddingbottom="[0,0,0,0]"
                data-paddingleft="[0,0,0,0]"

                style="z-index: 6; white-space: nowrap; font-size: 110px; line-height: 110px; font-weight: 700; color: rgba(255, 255, 255, 1.00);border-width:0px;letter-spacing:-7px;">Summer Collection
            </div> --}}
          </li>
          <!-- END SLIDE  -->
        @endforeach

      </ul>
      <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
    </div>
  </div>
</div>
<!-- End Revolution Slider -->

  {{-- <section class="g-bg-secondary ">
    <div class="container g-pt-100 g-pb-40">
      <div class="row justify-content-center g-mb-60">
        <div class="col-lg-7">
          <!-- Heading -->
          <div class="text-center">
            <h2 class="h3 g-color-black text-uppercase mb-2">Our services</h2>
            <div class="d-inline-block g-width-35 g-height-2 g-bg-primary mb-2"></div>
            <p class="lead mb-0">We are a creative person focusing on culture, luxury, editorial &amp; art. Somewhere between sophistication and simplicity.</p>
          </div>
          <!-- End Heading -->
        </div>
      </div>

      <!-- Icon Blocks -->
      <div class="row">
        <div class="col-lg-4 align-self-center g-mb-30">
          <!-- Icon Blocks -->
          <div class="media g-mb-30">
            <div class="d-flex mr-4">
              <span class="u-icon-v3 u-icon-size--sm g-color-white g-bg-primary rounded-circle">
                <i class="icon-education-087 u-line-icon-pro"></i>
              </span>
            </div>
            <div class="media-body">
              <h3 class="h5 g-color-black mb-20">Creative ideas</h3>
              <p class="g-color-gray-dark-v4">We strive to embrace and drive change in our industry which allows us to keep our clients relevant.</p>
            </div>
          </div>
          <!-- End Icon Blocks -->

          <!-- Icon Blocks -->
          <div class="media g-mb-30">
            <div class="d-flex mr-4">
              <span class="u-icon-v3 u-icon-size--sm g-color-white g-bg-primary rounded-circle">
                <i class="icon-education-035 u-line-icon-pro"></i>
              </span>
            </div>
            <div class="media-body">
              <h3 class="h5 g-color-black mb-20">Excellent features</h3>
              <p class="g-color-gray-dark-v4">We strive to embrace and drive change in our industry which allows us to keep our clients relevant.</p>
            </div>
          </div>
          <!-- End Icon Blocks -->
        </div>

        <div class="col-lg-4 align-self-center g-mb-30">
          <!-- Icon Blocks -->
          <div class="media g-mb-30">
            <div class="d-flex mr-4">
              <span class="u-icon-v3 u-icon-size--sm g-color-white g-bg-primary rounded-circle">
                <i class="icon-finance-256 u-line-icon-pro"></i>
              </span>
            </div>
            <div class="media-body">
              <h3 class="h5 g-color-black mb-20">Clean code</h3>
              <p class="g-color-gray-dark-v4">We strive to embrace and drive change in our industry which allows us to keep our clients relevant.</p>
            </div>
          </div>
          <!-- End Icon Blocks -->

          <!-- Icon Blocks -->
          <div class="media g-mb-30">
            <div class="d-flex mr-4">
              <span class="u-icon-v3 u-icon-size--sm g-color-white g-bg-primary rounded-circle">
                <i class="icon-finance-009 u-line-icon-pro"></i>
              </span>
            </div>
            <div class="media-body">
              <h3 class="h5 g-color-black mb-20">Modern design</h3>
              <p class="g-color-gray-dark-v4">We strive to embrace and drive change in our industry which allows us to keep our clients relevant.</p>
            </div>
          </div>
          <!-- End Icon Blocks -->
        </div>

        <div class="col-lg-4 align-self-center g-mb-30">
          <div class="g-px-10">
            <img class="img-fluid" src="frontend/assets/img-temp/900x700/img1.png" alt="Image Description">
          </div>
        </div>
      </div>
      <!-- End Icon Blocks -->
    </div>
  </section> --}}

  <!-- Categories -->
  <div class="container g-pt-100 g-pb-70">
    <div class="row g-mx-minus-10">
      <div class="col-sm-6 col-md-4 g-px-10 g-mb-30">
        <article class="u-block-hover">
          <img class="w-100 u-block-hover__main--zoom-v1 g-mb-minus-8" src="/frontend/assets/img-temp/img1.jpg" alt="Image Description">
          <div class="g-pos-abs g-bottom-30 g-left-30">
            <span class="d-block g-color-black">Collections</span>
            <h2 class="h1 mb-0">Women</h2>
          </div>
          <a class="u-link-v2" href="#!"></a>
        </article>
      </div>

      <div class="col-sm-6 col-md-4 g-px-10 g-mb-30">
        <article class="u-block-hover">
          <img class="w-100 u-block-hover__main--zoom-v1 g-mb-minus-8" src="/frontend/assets/img-temp/img1.jpg" alt="Image Description">
          <div class="g-pos-abs g-bottom-30 g-left-30">
            <span class="d-block g-color-black">Collections</span>
            <h2 class="h1 mb-0">Children</h2>
          </div>
          <a class="u-link-v2" href="#!"></a>
        </article>
      </div>

      <div class="col-sm-6 col-md-4 g-px-10 g-mb-30">
        <article class="u-block-hover">
          <img class="w-100 u-block-hover__main--zoom-v1 g-mb-minus-8" src="/frontend/assets/img-temp/img1.jpg" alt="Image Description">
          <div class="g-pos-abs g-bottom-30 g-left-30">
            <span class="d-block g-color-black">Collections</span>
            <h2 class="h1 mb-0">Men</h2>
          </div>
          <a class="u-link-v2" href="#!"></a>
        </article>
      </div>
    </div>
  </div>
  <!-- End Categories -->
  <!-- Icon Blocks -->
@if(count($featured_products) > 0)
  <div class="container g-pb-100">
    <div class="text-center mx-auto g-max-width-600 g-mb-50">
      <h2 class="g-color-black mb-4">Featured Products</h2>
      <p class="lead">We want to create a range of beautiful, practical and modern outerwear that doesn't cost the earth – but let's you still live life in style.</p>
    </div>

    <div id="carouselCus1" class="js-carousel g-pb-100 g-mx-minus-10"
         data-infinite="true"
         data-slides-show="4"
         data-lazy-load="ondemand"
         data-arrows-classes="u-arrow-v1 g-pos-abs g-bottom-0 g-width-45 g-height-45 g-color-gray-dark-v5 g-bg-secondary g-color-white--hover g-bg-primary--hover rounded"
         data-arrow-left-classes="fa fa-angle-left g-left-10"
         data-arrow-right-classes="fa fa-angle-right g-right-10"
         data-pagi-classes="u-carousel-indicators-v1 g-absolute-centered--x g-bottom-20 text-center">
        @foreach($featured_products as $p)
          <div class="js-slide">
            <div class="g-px-10">
              <!-- Product -->
              @foreach($images->where('product_id',$p->id)->slice(0,1) as $i)
                <figure class="g-pos-rel g-mb-20">
                  <img class="img-fluid" data-lazy="{{asset('product-images/180x180'.'/'.$i->image)}}" alt="Image Description">

                  <!-- <figcaption class="w-100 g-bg-primary g-bg-black--hover text-center g-pos-abs g-bottom-0 g-transition-0_2 g-py-5">
                    <a class="g-color-white g-font-size-11 text-uppercase g-letter-spacing-1 g-text-underline--none--hover" href="#!">New Arrival</a>
                  </figcaption> -->
                </figure>
              @endforeach
              <div class="media">
                <!-- Product Info -->
                <div class="d-flex flex-column">
                  <h4 class="h6 g-color-black mb-1">
                    <a class="u-link-v5 g-color-black g-color-primary--hover" href="{{URL::to('products'.'/'.$p->slug)}}">
                      {{$p->product_name}}
                    </a>
                  </h4>
                  {{-- <a class="d-inline-block g-color-gray-dark-v5 g-font-size-13" href="#!">Man</a> --}}
                  <span class="d-block g-color-black g-font-size-17">$52.00</span>
                </div>
                <!-- End Product Info -->

                <!-- Products Icons -->
                <ul class="list-inline media-body text-right">
                  <li class="list-inline-item align-middle mx-0">
                    <a class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-color-primary--hover g-font-size-15 rounded-circle" 
                      href="{{URL::to('order'.'/'.$p->slug)}}"
                       data-toggle="tooltip"
                       data-placement="top"
                       title="Place Order">
                      <i class="icon-medical-022 u-line-icon-pro"></i>
                    </a>
                  </li>
                </ul>
                <!-- End Products Icons -->
              </div>
              <!-- End Product -->
            </div>
          </div>
        @endforeach
    </div>
  </div>
  {{-- <section class="g-bg-secondary">
    <div class="container g-pt-5 g-pb-50">
      <!-- Icon Blocks -->
      <div class="row no-gutters">
        <div class="col-sm-6 col-lg-3">
          <div class="g-pr-40 g-mt-20">
            <div class="g-mb-30">
              <h2 class="h2 g-color-black g-font-weight-600 g-line-height-1_2 mb-4">What products<br>we provide?</h2>
              <p class="g-font-weight-300 g-font-size-16">The time has come to bring those ideas and plans to life. This is where we really begin to visualize your napkin sketches and make them into beautiful pixels.</p>
            </div>
          </div>
        </div>

        <div class="col-lg-9">
          <div class="js-carousel" data-infinite="true" data-slides-show="3" data-slides-scroll="3" data-center-mode="true" data-center-padding="1px" data-pagi-classes="u-carousel-indicators-v1 g-absolute-centered--x g-bottom-minus-30" data-responsive='[{
             "breakpoint": 992,
             "settings": {
               "slidesToShow": 2
             }
           }, {
             "breakpoint": 576,
             "settings": {
               "slidesToShow": 1
             }
           }]'>
        @foreach($featured_products as $p)
            <div class="js-slide">
              <!-- Icon Blocks -->
              <div class="u-shadow-v21--hover g-brd-around g-brd-gray-light-v3 g-brd-left-none g-brd-transparent--hover g-bg-white--hover g-transition-0_3 g-cursor-pointer g-px-30 g-pt-30 g-pb-50 g-ml-minus-1">
                <div class="mb-4">
                  @foreach($images->where('product_id',$p->id)->slice(0,1) as $i)
                  <span class="u-icon-v3 u-shadow-v19 g-bg-white g-color-primary rounded-circle mb-4">
                    <img src="{{asset('product-images/180x180'.'/'.$i->image)}}" style="height:70px; width:90px;"/>
              </span>
              @endforeach
            <h3 class="h5 g-color-black g-font-weight-600 mb-3">{{$p->product_name}}</h3>
            <p>{{str_limit($p->product_description),70}}</p>
                </div>
              <a class="g-brd-bottom g-brd-gray-dark-v5 g-brd-primary--hover g-color-gray-dark-v5 g-color-primary--hover g-font-weight-600 g-font-size-12 text-uppercase g-text-underline--none--hover" href="{{URL::to('products'.'/'.$p->slug)}}">View Product</a>
              </div>
              <!-- End Icon Blocks -->
            </div>
            @endforeach
          </div>
        </div>
      </div>
      <!-- End Icon Blocks -->
    </div>
  </section> --}}
@endif
  <!-- End Icon Blocks -->

  <!-- Mockup Block -->
  <section class="container g-pt-10 g-pb-40">
    <div class="text-center g-mb-20">
      <h2 class="h1 g-color-black g-font-weight-600">Digital Printing Magic</h2>
      <p class="lead">This is where we begin to visualize your napkin sketches and make them into visible and touchable pixels.</p>
    </div>

    <div class="row">
      <div class="col-md-3 g-mb-30">
        <span class="u-icon-v3 g-color-cyan g-bg-cyan-opacity-0_1 rounded-circle g-mb-25">
      <i class="icon-education-089 u-line-icon-pro"></i>
    </span>
        <div class="d-inline-block g-width-70 g-height-2 g-bg-cyan g-pos-rel g-top-5 g-right-minus-25 g-mr-minus-20"></div>
        <h2 class="h5 g-color-black g-font-weight-600 mb-3">Design is Yours</h2>
        <p>Bring your design to us and we assure your you get the best viable output.It time to turn your idea into next big thing</p>
      </div>

      <div class="col-md-6 g-mb-30">
        <img class="img-fluid" src="{{asset('frontend/assets/img-temp/stock/background4.jpg')}}" alt="Image Description">
      </div>

      <div class="col-md-3 mt-auto g-mb-30">
        <div class="d-inline-block g-width-70 g-height-2 g-bg-red g-pos-rel g-top-5 g-left-minus-25 g-mr-minus-20"></div>
        <span class="u-icon-v3 g-color-red g-bg-red-opacity-0_1 rounded-circle g-mb-25">
      <i class="icon-education-088 u-line-icon-pro"></i>
    </span>
        <h2 class="h5 g-color-black g-font-weight-600 mb-3">More than 200+ clients</h2>
        <p>We aim high at being focused on building relationships with our clients and community.</p>
      </div>
    </div>
  </section>
  <!-- End Mockup Block -->

 
  <!-- Testimonials Advanced -->
  @if($testimonials != null)
  <section class="container g-py-50">
    <div class="row justify-content-center g-mb-20">
      <div class="col-lg-7">
        <!-- Heading -->
        <div class="text-center">
          <h2 class="h3 g-color-black text-uppercase mb-2">What people say</h2>
          <div class="d-inline-block g-width-35 g-height-2 g-bg-primary mb-2"></div>
        </div>
        <!-- End Heading -->
      </div>
    </div>

    <!-- Testimonials Advanced -->
    <div class="js-carousel" data-infinite="true" data-arrows-classes="u-arrow-v1 g-width-50 g-height-50 g-brd-1 g-brd-style-solid g-brd-gray-light-v1 g-brd-primary--hover g-color-gray-dark-v5 g-bg-primary--hover g-color-white--hover g-absolute-centered--y rounded-circle g-mt-minus-25"
    data-arrow-left-classes="fa fa-angle-left g-left-0" data-arrow-right-classes="fa fa-angle-right g-right-0">
    @foreach($testimonials as $testi)
      <div class="js-slide">
        <div class="row justify-content-center">
          <div class="col-lg-7">
            <!-- Testimonial Advanced -->
            <div class="g-pos-rel g-pt-20">
              <em class="g-color-gray-light-v5 g-font-size-120 g-pos-abs g-top-minus-15 g-left-minus-15 g-z-index-minus-1">
                  &#8220;
                </em>
              <blockquote class="lead g-font-style-italic g-line-height-2 g-pl-30 g-mb-30">{{$testi->content}}</blockquote>

              <div class="media">
              <img class="d-flex align-self-center g-width-50 g-height-50 rounded-circle mr-3" src="{{asset('landing_images'.'/'.$testi->photo)}}" alt="{{$testi->photo}}">

                <div class="media-body align-self-center">
                <h4 class="h5 g-font-weight-700 g-mb-0">{{$testi->name}}</h4>
                <span class="d-block g-color-gray-dark-v4">{{$testi->profession}}</span>
                </div>
              </div>
            </div>
            <!-- End Testimonial Advanced -->
          </div>
        </div>
      </div>
      @endforeach
    </div>
    <!-- End Testimonials Advanced -->
  </section>
  @endif



  <!-- Call To Action -->
  <section class="g-bg-primary g-color-white g-pa-30" style="background-image: url({{asset('frontend/assets/img/bg/pattern5.png')}}">
    <div class="d-md-flex justify-content-md-center text-center">
      <div class="align-self-md-center">
        <p class="lead g-font-weight-400 g-mr-20--md g-mb-15 g-mb-0--md">Send Us your design and we will print it for you</p>
      </div>
      <div class="align-self-md-center">
      <a class="btn btn-lg u-btn-white text-uppercase g-font-weight-600 g-font-size-12" target="_blank" href="{{URL::to('contact')}}">Contact Us</a>
      </div>
    </div>
  </section>

  <!-- Demo modal window -->
{{-- <div id="modal-type-aftersometime" class="js-autonomous-popup text-left g-max-width-600 g-bg-white g-overflow-y-auto g-pa-20" style="display: none;" data-modal-type="aftersometime" data-open-effect="zoomIn" data-close-effect="zoomOut" data-delay="2000" data-speed="500">
  <button type="button" class="close" onclick="Custombox.modal.close();">
    <i class="hs-icon hs-icon-close"></i>
  </button>
  <h4>Holi Offer!</h4>
  <br>
  <div class="g-pb-10">
    <img class="img-fluid" src="{{asset('frontend/assets/img-temp/stock/offer.jpg')}}" style="height:300px; width:600px;" alt="Image Description">
  </div>
  <p>Here we can add any special offers (like holi offer or any discount offers) and anything new the company has to offer which will pop up after sometimes.</p>
  
</div> --}}
<!-- End Demo modal window -->
    
@endsection

@section('js')

  <script>
    $.HSCore.components.HSCarousel.init('[class*="js-carousel"]');

    $('#carouselCus1').slick('setOption', 'responsive', [{
        breakpoint: 1200,
        settings: {
          slidesToShow: 4
        }
    }, {
      breakpoint: 992,
      settings: {
        slidesToShow: 3
      }
    }, {
      breakpoint: 768,
      settings: {
        slidesToShow: 2
      }
    }], true);
    
    // initialization of revolution slider
    var tpj = jQuery;

    var revapi1014;
    tpj(document).ready(function () {
      if (tpj("#rev_slider_1014_1").revolution == undefined) {
        revslider_showDoubleJqueryError("#rev_slider_1014_1");
      } else {
        revapi1014 = tpj("#rev_slider_1014_1").show().revolution({
          sliderType: "standard",
          jsFileLocation: "revolution/js/",
          sliderLayout: "fullscreen",
          dottedOverlay: "none",
          delay: 9000,
          navigation: {
            keyboardNavigation: "off",
            keyboard_direction: "horizontal",
            mouseScrollNavigation: "off",
            mouseScrollReverse: "default",
            onHoverStop: "off",
            touch: {
              touchenabled: "on",
              swipe_threshold: 75,
              swipe_min_touches: 1,
              swipe_direction: "horizontal",
              drag_block_vertical: false
            }
            ,
            arrows: {
              style: "uranus",
              enable: true,
              hide_onmobile: true,
              hide_under: 768,
              hide_onleave: false,
              tmp: '',
              left: {
                h_align: "left",
                v_align: "center",
                h_offset: 20,
                v_offset: 0
              },
              right: {
                h_align: "right",
                v_align: "center",
                h_offset: 20,
                v_offset: 0
              }
            }
          },
          parallax: {
            type: "mouse",
            origo: "slidercenter",
            speed: 2000,
            levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50],
            disable_onmobile: "on"
          },
          responsiveLevels: [1240, 1024, 778, 480],
          visibilityLevels: [1240, 1024, 778, 480],
          gridwidth: [1240, 1024, 778, 480],
          gridheight: [868, 768, 960, 600],
          lazyType: "none",
          shadow: 0,
          spinner: "off",
          stopLoop: "on",
          stopAfterLoops: 0,
          stopAtSlide: 1,
          shuffle: "off",
          autoHeight: "off",
          fullScreenAutoWidth: "off",
          fullScreenAlignForce: "off",
          fullScreenOffsetContainer: "",
          fullScreenOffset: "60px",
          disableProgressBar: "on",
          hideThumbsOnMobile: "off",
          hideSliderAtLimit: 0,
          hideCaptionAtLimit: 0,
          hideAllCaptionAtLilmit: 0,
          debugMode: false,
          fallbacks: {
            simplifyAll: "off",
            nextSlideOnWindowFocus: "off",
            disableFocusListener: false
          }
        });
      }

      RsTypewriterAddOn(tpj, revapi1014);
    });
  </script>
@endsection
