@extends('layouts.main') 
@section('body')
@section('title','| '.$product->product_name)


  <!-- Breadcrumbs -->
  <section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
    <div class="container">
      <ul class="u-list-inline">
        <li class="list-inline-item g-mr-5">
          <a class="u-link-v5 g-color-text" href="{{URL::to('home')}}">Home</a>
          <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
        </li>
        <li class="list-inline-item g-color-primary">
          <span>{{$product->product_name}}</span>
        </li>
      </ul>
    </div>
  </section>
  <!-- End Breadcrumbs -->

  <!-- Product Description -->
  <div class="container g-pt-50 g-pb-50">
    <div class="row">
      <div class="col-lg-7">
        <!-- Carousel -->
        <div id="carouselCus1" class="js-carousel g-pt-10 g-mb-10"
          data-infinite="true"
          data-fade="true"
          data-arrows-classes="u-arrow-v1 g-brd-around g-brd-white g-absolute-centered--y g-width-45 g-height-45 g-font-size-14 g-color-white g-color-primary--hover rounded-circle"
          data-arrow-left-classes="fa fa-angle-left g-left-40"
          data-arrow-right-classes="fa fa-angle-right g-right-40"
          data-nav-for="#carouselCus2">
            @foreach($images as $img)
              <div class="js-slide g-bg-cover g-bg-black-opacity-0_1--after">
                <img class="img-fluid w-100" src="{{asset('product-images/750x500'.'/'.$img->image)}}" alt="{{$product->product_name}}">
              </div>
            @endforeach
            
        </div>
        @if(count($images) > 1)
          <div id="carouselCus2" class="js-carousel text-center u-carousel-v3 g-mx-minus-5"
            data-center-mode="true"
            data-slides-show="3"
            data-is-thumbs="true"
            data-focus-on-select="true"
            data-nav-for="#carouselCus1">
            @foreach($images as $img)
              <div class="js-slide g-cursor-pointer g-px-5">
                <img class="img-fluid" src="{{asset('product-images/180x180'.'/'.$img->image)}}" alt="{{$product->product_name}}">
              </div>
            @endforeach
          </div>
        @endif
        <!-- End Carousel -->
        
      </div>

      <div class="col-lg-5">
        <div class="g-px-40--lg g-pt-10">
          <!-- Product Info -->
          <div class="g-mb-30">
            <h1 class="g-font-weight-300 mb-4">{{$product->product_name}}</h1>
            {{$product->product_description}}
          </div>
          <!-- End Product Info --> 

          <!-- Price -->
          <div class="g-mb-30">
            <h2 class="g-color-gray-dark-v5 g-font-weight-400 g-font-size-12 text-uppercase mb-2">Price</h2>
            <span class="g-color-black g-font-weight-500 g-font-size-30 mr-2">$99.00</span>
            {{-- <s class="g-color-gray-dark-v4 g-font-weight-500 g-font-size-16">$101.00</s> --}}
          </div>
          <!-- End Price -->

          <!-- Accordion -->
          <div id="accordion-01" role="tablist" aria-multiselectable="true">
            <div id="accordion-01-heading-01" class="g-brd-y g-brd-gray-light-v3 py-3" role="tab">
              <h5 class="g-font-weight-400 g-font-size-default mb-0">
                <a class="d-block g-color-gray-dark-v5 g-text-underline--none--hover" href="#accordion-01-body-01" data-toggle="collapse" data-parent="#accordion-01" aria-expanded="false" aria-controls="accordion-01-body-01">Details
                  <span class="float-right g-pos-rel g-top-3 mr-1 fa fa-angle-down"></span></a>
              </h5>
            </div>
            <div id="accordion-01-body-01" class="collapse show" role="tabpanel" aria-labelledby="accordion-01-heading-01">
              <div class="g-py-10">
                @if(count($product->details) != null)
                  <?php $details = json_decode($product->details) ?>
                      
                  <div class="g-mb-0 g-mb-30--md g-mx-15">
                    <!-- List -->
                    <ul class="list-unstyled g-color-text">
                      @foreach ($details as $c)
                        <li class="g-brd-bottom--dashed g-brd-gray-light-v3 pt-1 mb-3">
                          <span>{{ $c->name }}:</span>
                          <span class="float-right g-color-black">{{ $c->value[0] }}</span>
                        </li>
                      @endforeach
                      
                    </ul>
                    <!-- End List -->
                  </div>
                @endif
              </div>
            </div>
          </div>
          <!-- End Accordion -->

          <!-- Colour -->
          @if($product->colors != "[]")
            <div class="d-flex justify-content-between align-items-center g-brd-bottom g-brd-gray-light-v3 py-3" role="tab">
              <h5 class="g-color-gray-dark-v5 g-font-weight-400 g-font-size-default mb-0">Colour</h5>

              <!-- Checkbox -->
              <ul class="list-inline mb-0">
                  
                @foreach(json_decode($product->colors) as $color)
                  <li class="list-inline-item g-mx-10">
                    <label class="form-check-inline u-check">
                      <span class="d-block g-brd-transparent rounded-circle g-absolute-centered--y g-left-0 g-mt-3">
                      <i style="background-color: {{$color->value}};" class="d-block g-absolute-centered g-width-12 g-height-12 rounded-circle"></i>
                      </span>
                    </label>
                  </li>
                @endforeach
              </ul>
              <!-- End Checkbox -->
            </div>
          @endif
          <!-- End Colour -->

          <div class="row g-mx-minus-5 g-mb-20">
            <div class="col g-px-5 g-mb-10">
                <a class="g-text-underline--none--hover" href="{{URL::to('order'.'/'.$product->slug)}}">
                <button  class="btn btn-block u-btn-primary g-font-size-12  text-uppercase g-py-15 g-px-25" type="button">
                  Place Order
                  <i class="align-middle ml-2 icon-finance-100 u-line-icon-pro"></i>
                </button>
            </a>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Product Description -->


  <!-- Description -->
  {{-- <div class="container">
    <div class="g-brd-y g-brd-gray-light-v4 g-pt-100 g-pb-70">
      <h2 class="h4 mb-3">Details</h2>

      <div class="row">
        

        @if(count($product->details) != null)
          @foreach ($details->chunk(5) as $chunk)
              
            <div class="col-md-4 g-mb-0 g-mb-30--md">
              <!-- List -->
              <ul class="list-unstyled g-color-text">
                @foreach ($chunk as $c)
                  <li class="g-brd-bottom--dashed g-brd-gray-light-v3 pt-1 mb-3">
                    <span>{{ $c->name }}:</span>
                    <span class="float-right g-color-black">{{ $c->value[0] }}</span>
                  </li>
                @endforeach
                
              </ul>
              <!-- End List -->
            </div>
          @endforeach
        @endif
      </div>
    </div>
  </div> --}}
  <!-- End Description -->

  <!-- Products -->
  <div class="container g-pt-100 g-pb-70">
    <div class="text-center mx-auto g-max-width-600 g-mb-50">
      <h2 class="g-color-black mb-4">Other Products</h2>
      <p class="lead">We want to create a range of beautiful, practical and modern outerwear that doesn't cost the earth – but let's you still live life in style.</p>
    </div>

    <!-- Products -->
    <div class="row">
      <div class="col-6 col-lg-3 g-mb-30">
        <!-- Product -->
        <figure class="g-pos-rel g-mb-20">
          <img class="img-fluid" src="assets/img-temp/480x700/img1.jpg" alt="Image Description">

          <figcaption class="w-100 g-bg-primary g-bg-black--hover text-center g-pos-abs g-bottom-0 g-transition-0_2 g-py-5">
            <a class="g-color-white g-font-size-11 text-uppercase g-letter-spacing-1 g-text-underline--none--hover" href="#!">New Arrival</a>
          </figcaption>
        </figure>

        <div class="media">
          <!-- Product Info -->
          <div class="d-flex flex-column">
            <h4 class="h6 g-color-black mb-1">
              <a class="u-link-v5 g-color-black g-color-primary--hover" href="#!">
                Summer shorts
              </a>
            </h4>
            <a class="d-inline-block g-color-gray-dark-v5 g-font-size-13" href="#!">Man</a>
            <span class="d-block g-color-black g-font-size-17">$52.00</span>
          </div>
          <!-- End Product Info -->

          <!-- Products Icons -->
          <ul class="list-inline media-body text-right">
            <li class="list-inline-item align-middle mx-0">
              <a class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-color-primary--hover g-font-size-15 rounded-circle" href="#!"
                  data-toggle="tooltip"
                  data-placement="top"
                  title="Add to Cart">
                <i class="icon-finance-100 u-line-icon-pro"></i>
              </a>
            </li>
            <li class="list-inline-item align-middle mx-0">
              <a class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-color-primary--hover g-font-size-15 rounded-circle" href="#!"
                  data-toggle="tooltip"
                  data-placement="top"
                  title="Add to Wishlist">
                <i class="icon-medical-022 u-line-icon-pro"></i>
              </a>
            </li>
          </ul>
          <!-- End Products Icons -->
        </div>
        <!-- End Product -->
      </div>

      <div class="col-6 col-lg-3 g-mb-30">
        <!-- Product -->
        <figure class="g-pos-rel g-mb-20">
          <img class="img-fluid" src="assets/img-temp/480x700/img2.jpg" alt="Image Description">

          <span class="u-ribbon-v1 g-width-40 g-height-40 g-color-white g-bg-primary g-font-size-13 text-center text-uppercase g-rounded-50x g-top-10 g-right-minus-10 g-px-2 g-py-10">-40%</span>
        </figure>

        <div class="media">
          <!-- Product Info -->
          <div class="d-flex flex-column">
            <h4 class="h6 g-color-black mb-1">
              <a class="u-link-v5 g-color-black g-color-primary--hover" href="#!">
                Stylish shirt
              </a>
            </h4>
            <a class="d-inline-block g-color-gray-dark-v5 g-font-size-13" href="#!">Woman</a>
            <span class="d-block g-color-black g-font-size-17">$99.00</span>
          </div>
          <!-- End Product Info -->

          <!-- Products Icons -->
          <ul class="list-inline media-body text-right">
            <li class="list-inline-item align-middle mx-0">
              <a class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-color-primary--hover g-font-size-15 rounded-circle" href="#!"
                  data-toggle="tooltip"
                  data-placement="top"
                  title="Add to Cart">
                <i class="icon-finance-100 u-line-icon-pro"></i>
              </a>
            </li>
            <li class="list-inline-item align-middle mx-0">
              <a class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-color-primary--hover g-font-size-15 rounded-circle" href="#!"
                  data-toggle="tooltip"
                  data-placement="top"
                  title="Add to Wishlist">
                <i class="icon-medical-022 u-line-icon-pro"></i>
              </a>
            </li>
          </ul>
          <!-- End Products Icons -->
        </div>
        <!-- End Product -->
      </div>

      <div class="col-6 col-lg-3 g-mb-30">
        <!-- Product -->
        <figure class="g-pos-rel g-mb-20">
          <img class="img-fluid" src="assets/img-temp/480x700/img3.jpg" alt="Image Description">

          <figcaption class="w-100 g-bg-lightred text-center g-pos-abs g-bottom-0 g-transition-0_2 g-py-5">
            <span class="g-color-white g-font-size-11 text-uppercase g-letter-spacing-1">Sold Out</a>
          </figcaption>
        </figure>

        <div class="media">
          <!-- Product Info -->
          <div class="d-flex flex-column">
            <h4 class="h6 g-color-black mb-1">
              <a class="u-link-v5 g-color-black g-color-primary--hover" href="#!">
                Classic jacket
              </a>
            </h4>
            <a class="d-inline-block g-color-gray-dark-v5 g-font-size-13" href="#!">Man</a>
            <span class="d-block g-color-black g-font-size-17">$49.99</span>
          </div>
          <!-- End Product Info -->

          <!-- Products Icons -->
          <ul class="list-inline media-body text-right">
            <li class="list-inline-item align-middle mx-0">
              <a class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-color-primary--hover g-font-size-15 rounded-circle" href="#!"
                  data-toggle="tooltip"
                  data-placement="top"
                  title="Add to Cart">
                <i class="icon-finance-100 u-line-icon-pro"></i>
              </a>
            </li>
            <li class="list-inline-item align-middle mx-0">
              <a class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-color-primary--hover g-font-size-15 rounded-circle" href="#!"
                  data-toggle="tooltip"
                  data-placement="top"
                  title="Add to Wishlist">
                <i class="icon-medical-022 u-line-icon-pro"></i>
              </a>
            </li>
          </ul>
          <!-- End Products Icons -->
        </div>
        <!-- End Product -->
      </div>

      <div class="col-6 col-lg-3 g-mb-30">
        <!-- Product -->
        <figure class="g-pos-rel g-mb-20">
          <img class="img-fluid" src="assets/img-temp/480x700/img4.jpg" alt="Image Description">
        </figure>

        <div class="media">
          <!-- Product Info -->
          <div class="d-flex flex-column">
            <h4 class="h6 g-color-black mb-1">
              <a class="u-link-v5 g-color-black g-color-primary--hover" href="#!">
                Wool lined parka
              </a>
            </h4>
            <a class="d-inline-block g-color-gray-dark-v5 g-font-size-13" href="#!">Woman</a>
            <span class="d-block g-color-black g-font-size-17">$82.37</span>
          </div>
          <!-- End Product Info -->

          <!-- Products Icons -->
          <ul class="list-inline media-body text-right">
            <li class="list-inline-item align-middle mx-0">
              <a class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-color-primary--hover g-font-size-15 rounded-circle" href="#!"
                  data-toggle="tooltip"
                  data-placement="top"
                  title="Add to Cart">
                <i class="icon-finance-100 u-line-icon-pro"></i>
              </a>
            </li>
            <li class="list-inline-item align-middle mx-0">
              <a class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-color-primary--hover g-font-size-15 rounded-circle" href="#!"
                  data-toggle="tooltip"
                  data-placement="top"
                  title="Add to Wishlist">
                <i class="icon-medical-022 u-line-icon-pro"></i>
              </a>
            </li>
          </ul>
          <!-- End Products Icons -->
        </div>
        <!-- End Product -->
      </div>
    </div>
    <!-- End Products -->
  </div>
  <!-- End Products -->
@endsection

@section('js')

<script>
  $.HSCore.components.HSCarousel.init('[class*="js-carousel"]');
</script>
@endsection