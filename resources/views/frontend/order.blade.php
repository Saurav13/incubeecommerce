@extends('layouts.main')
@section('body')
@section('title','| Order')
<div class="container">			
        <div class="text-center">
                <img src="{{asset('product-images/180x180'.'/'.$image->image)}}" alt="Shrestha Digital Printers"  style="height:150px;" />
        <h3>Place your order for "{{$product->product_name}}"</h3>
      
        </div>
</div>	
<div  class="container">
        <form action="{{route('general.order')}}" method="POST" enctype="multipart/form-data" class="g-brd-around g-brd-gray-light-v4 g-pa-30 g-mb-30">
                {{csrf_field()}}
                <h3 class="g-mb-15">Product Information</h3>
                <p>Note *: If the following values do not match your requirement please specify your requirement in the message box below</p>
                <hr class="g-brd-gray-light-v4 g-mx-minus-30">

                <div class="form-group">
                        <label class="g-mb-10"><h4>Quantity</h4></label>
                        <div class="js-quantity input-group u-quantity-v1 g-width-170 g-brd-primary--focus">
                                <div class="js-minus input-group-addon d-flex align-items-center g-width-55 g-color-gray g-bg-grey-light-v3 rounded-0">
                                        <i class="fa fa-minus"></i>
                                </div>
                                <input name="quantity" class="js-result form-control text-center rounded-0 g-pa-15" type="text" value="0">
                                <div class="js-plus input-group-addon d-flex align-items-center g-width-55 g-color-gray g-bg-grey-light-v3 rounded-0">
                                        <i class="fa fa-plus"></i>
                                </div>
                        </div>
                </div>

                <hr class="g-brd-gray-light-v4 g-mx-minus-30">
                <h3 class="g-mb-15">Your Information</h3>
                <p>Please enter your correct information details.</p>
                <hr class="g-brd-gray-light-v4 g-mx-minus-30">
                <div class="form-group g-mb-20">
                        <label class="g-mb-10" for="inputGroup1_1"><h3>Name*</h3></label>
                        <input id="inputGroup1_1" class="form-control form-control-md rounded-0" name="name" type="text" placeholder="Enter first name">
                        
                </div>
                <div class="form-group g-mb-20">
                        <label class="g-mb-10" for="inputGroup1_1"><h3>Your Email*</h3></label>
                        <input id="inputGroup1_1" class="form-control form-control-md rounded-0" name="email" type="email" placeholder="Enter email">
                        <small class="form-text text-muted g-font-size-default g-mt-10">We'll never share your email with anyone else.</small>
                </div>
                <div class="form-group g-mb-20">
                        <label class="g-mb-10" for="inputGroup1_1"><h3>Phone*</h3></label>
                        <input id="inputGroup1_1" class="form-control form-control-md rounded-0" type="text" placeholder="Enter a reachable phone number..." name="number">
                        <small class="form-text text-muted g-font-size-default g-mt-10">We'll never share your number with anyone else.</small>
                </div>
                <div class="form-group g-mb-20">
                        <label class="g-mb-10" for="inputGroup2_2"><h3>Message*</h3></label>
                        <textarea id="inputGroup2_2" name="message" class="form-control form-control-md rounded-0" rows="7" placeholder="A short description of what service you would like to use"></textarea>
                </div>
                <div class="form-group mb-20">
                        <label for="exampleFormControlFile1">Attach Files (optional)</label>
                        <input type="file" name="file" class="form-control-file" id="exampleFormControlFile1">
                </div>
                <input type="hidden" value="{{$product->product_name}}" name="product_name">
                <div class="form-group g-mb-20">
                        <input type="submit" class="btn btn-md btn-primary" id="submit"/>
                </div>

                <!-- Advanced File Input -->
                        <!-- End Advanced File Input -->
        </form>
    
</div>
@section('js')
<!-- JS Implementing Plugins -->
<script  src="{{asset('frontend/assets/vendor/jquery.filer/js/jquery.filer.min.js')}}"></script>
<!-- JS Unify -->
<script  src="{{asset('frontend/assets/js/helpers/hs.focus-state.js')}}"></script>
<script  src="{{asset('frontend/assets/js/components/hs.file-attachement.js')}}"></script>

<!-- JS Plugins Init. -->
<script >
  $(document).on('ready', function () {
      // initialization of forms
      $.HSCore.components.HSFileAttachment.init('.js-file-attachment');
      $.HSCore.helpers.HSFocusState.init();
    });
</script>
<!-- JS Unify -->
<script  src="{{asset('frontend/assets/js/components/hs.count-qty.js')}}"></script>

<!-- JS Plugins Init. -->
<script >
  $(document).on('ready', function () {
      // initialization of forms
      $.HSCore.components.HSCountQty.init('.js-quantity');
    });
</script>


      


@endsection
@endsection