@extends('layouts.admin')

@section('body')

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Message #{{ $order->id }}</h2>
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                <a class="btn btn-primary btn-min-width mr-1 mb-1 " href="{{ route('all.orders') }}">See all Orders</a>
                </ol>
            </div>
        </div>
    </div>
    <div class="content-body">
        <section class="card">
            <div id="invoice-template" class="card-block">
                <!-- Invoice Company Details -->
                <div id="invoice-company-details" class="row">
                    <div class="col-md-6 col-sm-12 text-xs-center text-md-left">
                        <ul class="px-0 list-unstyled">
                            <li><strong>Name      : </strong>{{ $order->name }}</li>
                            <li><strong>Email     : </strong>{{ $order->email }}</li>                                
                            <li><strong>Phone No. : </strong>{{ $order->number }}</li><br>
                            <li><span class="text-muted">Received Date :</span> {{ date('M j, Y', strtotime($order->created_at)) }}</li>
                        </ul>
                    </div>
                </div>
                <!--/ Invoice Company Details -->

                <!-- Invoice Footer -->
                <div id="invoice-items-details" class="pt-2">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="text-xs-center">
                                <strong>Product Ordered : </strong><b>{{ $order->product_name }}</b>
                            </div><br>
                            <input type="hidden" value="{{$order->id}}" id="order_id"/>
                            <ul class="px-0 list-unstyled">
                                @if($order->color != null)
                                    <li><strong>Color : </strong>{{ $order->color }}</li>
                                @endif    
                                @if($order->size != null)
                                    <li><strong>Size    : </strong>{{ $order->Size }}</li>  
                                @endif
                                
                                @if($order->details != null)
                                    @foreach(json_decode($order->details) as $key => $values)
                                    <li><strong>{{$key}}: 
                                        </strong>{{ $values }} 
                                    </li>  
                                    @endforeach   
                                @endif                      
                            </ul>
                            <p class="lead">Message:</p>
                            <p>{{ $order->message }}</p>
                        </div>
                    </div>
                </div>
                <!--/ Invoice Footer -->

            </div>
        </section>
    </div>

@section('js')
<script>
    $(document).ready(function(){
        var id = $('#order_id').val();
      
            $.ajax({
              type: "POST",
              url: "{{URL::to('admin/markseen/order')}}",
              data: { 
              id: id,
              _token:"{{csrf_token()}}"
        },
        success: function(result) {
            
        },
        error: function(result) {
            alert("Order was not marked as seen");
        }
    });
});
</script>
@endsection

@endsection
