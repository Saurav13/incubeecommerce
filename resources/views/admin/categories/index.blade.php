@extends('layouts.admin')

@section('body')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-xs-12 mb-1">
            <h2 class="content-header-title">Manage Categories</h2>
            <button class="btn btn-md btn-primary" data-toggle="modal" id="addCategoryButton" data-target="#addCategory"><i class="icon-plus-circle"></i> Add Category</button>
        </div>
        <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper col-xs-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{URL::to('admin/dashboard')}}">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">Manage Categories
                    </li>
                </ol>
            </div>
        </div>
    </div>
    @foreach($categories as $c)
        <section id="categories">
            <div class="row" >
                <div class="col-xs-12" >
                    <div class="card">
                        <div class="card-header">
                            <input type="hidden" id="CategoryName{{$c->id}}" value="{{$c->cat_name}}"/> 
                            
                            <h3 class="card-title"><a data-action="collapse"><i class="icon-minus4"></i></a><a data-action="collapse"> {{$c->cat_name}}</a>
                            </h3>
                            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><button class="btn btn-sm btn-outline-blue" data-toggle="modal" data-target="#editCategory" id="CategoryEdit{{$c->id}}"><i class="icon-edit2"></i> Edit</button> </li>
                                    <li> <button class="btn btn-sm btn-outline-red" id="deletecat{{$c->id}}"><i class="icon-remove"></i> Delete</button></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body collapse in" style="padding-top: 5px;">
                            <div class="card-block"  style="padding-top:5px;">
                                
                                <!-- Content types section start -->
                                <section id="content-types">
                                    <div class="row">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="col-xs-10">
                                                    @if(count($subcategories->where('cat_id','=',$c->id)) != null)
                                                        <h4>Manage Sub-categories under "{{$c->cat_name}}"</h4>
                                                    @else
                                                        <h4>No Subcategories added yet.</h4>
                                                    @endif
                                                </div>
                                                <div class="col-xs-2" style="padding-left: 0px;padding-right: 0px;">
                                                    <button disabled class="btn btn-md btn-primary" data-toggle="modal" id="addSubCategoryButton{{$c->id}}" data-target="#addSubCategory"><i class="icon-plus-circle"></i> Add Sub-Category</button>
                                                </div>
                                            </div>
                                        </div>
                                        @if(count($subcategories->where('cat_id','=',$c->id)) != null)
                                            <hr>
                                        @endif
                                        <div class="row match-height">
                                            @foreach($subcategories->where('cat_id','=',$c->id) as $sub)
                                                <div class="col-xl-3 col-md-3 col-sm-12">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            {{-- <img class="card-img-top img-fluid" id="sub_cat_image{{$sub->id}}" src="{{asset('subcategory-images/750x500'.'/'.$sub->featured_image)}}" alt="{{$sub->subcat_name}}"> --}}
                                                            <div class="card-block">
                                                                <h4 class="card-title" id="sub_cat_name{{$sub->id}}" align="center">{{$sub->subcat_name}}</h4>
                                                                <input type="hidden" id="subcat_cat_id{{$sub->id}}" value="{{$c->id}}"/>
                                                                <div class="col-xs-12">
                                                                    <div class="col-xs-6">
                                                                        <a href="#" data-toggle="modal" data-target="#editSubCategory" id="edit_sub_category{{$sub->id}}" class="btn btn-sm btn-primary"><i class="icon-edit2"></i> Edit</a>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <a href="#" id="delete_subcat{{$sub->id}}" class="btn btn-sm btn-danger"><i class="icon-remove"></i> Delete</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
                    
    @endforeach

    {{-- Add Category Modal --}}
    <div  class="modal fade" id="addCategory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel19" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form id="savechanges" class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="cattitle">Add Category</h4>
                </div>
                <div class="modal-body">
                    <input type="text" id="addCatgeoryName" class="form-control" placeholder="Name of Category"/>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" >Add Category</button>
                </div>
            </form>
        </div>
    </div>
    {{-- End Add Category Modal --}}
    {{-- Edit Category Modal --}}

    <div  class="modal fade" id="editCategory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel20" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form id="editchanges" class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Edit Category Name</h4>
                </div>
                <div class="modal-body">
                    <input type="text" id="editCatgeoryName" class="form-control" placeholder="Name of Category"/>
                </div>
                <input type="hidden" id="editcategoryid" value=""/>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" >Save Category</button>
                </div>
            </form>
        </div>
    </div>    
    {{-- EndEdit Category Modal --}}

    {{-- Add Sub-category Modal --}}

    <div class="modal fade" id="addSubCategory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel19" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="subcattitle">Add Sub-Category</h4>
                </div>
                <form id="subcat_store_form" action="{{route('subcategory.add')}}" enctype="multipart/form-data" method="POST"> 
                    <div class="modal-body">
                        {{csrf_field()}}
                            <div class="form-body">
                                <div class="form-group">
                                    <label for="subcat_name">Sub-category Name *</label>
                                    <input type="text" name="subcat_name" id="addSubCatgeoryName" class="form-control" placeholder="Name of Sub-category" required/>
                                </div>
                                <div class="form-group">
                                    <label for="featured_image">Select Featured Image</label>
                                    <input type="file" name="featured_image"/>
                                    <input type="hidden" id="category_id_sub" name="category_id"/>
                                </div>
                            </div>        
                        </div>
                    <div class="modal-footer">
                        <button type="submit" data-dismiss="modal"  class="btn btn-primary" id="savesubcat">Add Sub-category</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    {{-- End Sub-category Modal --}}
   {{-- Edit Sub-Category Modal --}}
   <div class="modal fade" id="editSubCategory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel19" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="subcattitle">Edit Sub-Category</h4>
                </div>
                <form id="subcat_edit_form" action="{{route('subcategory.edit')}}" enctype="multipart/form-data" method="POST"> 
                    <div class="modal-body">
                        {{csrf_field()}}
                        <div class="form-body">
                                <div class="form-group">
                                    <label for="subcat_name">Sub-category Name *:</label>
                                    <input type="text" name="subcat_name" id="editSubCatgeoryName" class="form-control" placeholder="Name of Sub-category" required/>
                                </div>
                                <div class="form-group">
                                    <label for="category">Select Category:</label>
                                    <select name="category" class="form-control">
                                        @foreach($categories as $c)
                                            <option value="{{$c->id}}" id="select_category{{$c->id}}">{{$c->cat_name}}</option>
                                        @endforeach
                                    </select>
                                        
                                </div>
                                <div class="form-group">
                                    <label for="featured_image">Select Featured Image:</label>
                                    <input type="file" name="featured_image"/>
                                    <input type="hidden" id="my_subcat_id" name="id"/>

                                </div>
                        </div>        
                    </div>
                        
                    <div class="modal-footer">
                        <button type="submit" data-dismiss="modal"  class="btn btn-primary" id="editsavesubcat">Add Sub-category</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
   {{--End Edit Sub-Category Modal --}}


@endsection

@section('js')
    <script>

        $(document).ready(function(){
            $("[id*='CategoryEdit']").click(function(){
                var id = $(this).attr("id").slice(12);
                var catname=$('#CategoryName'+id).val();
                $('#editcategoryid').val(id);
                $('#editCatgeoryName').val(catname);
            });

            $('#savechanges').on('submit', function(event){
                event.preventDefault();

                var name = $('#addCatgeoryName').val();
                $.post("{{route('category.add')}}",{'name':name,'_token':$('input[name=_token]').val()},    function(data){
                    swal({
                        title: "Success",
                        text: "The Category was added",
                        type: "success",
                        showCancelButton: true,
                    }).then(function(){
                        location.reload();
                    })  
                })
                .fail(function(response) {
                    alert('Error: The given data is Invalid');
                });
            
            });

            $('#editchanges').on('submit', function(event){
                event.preventDefault();
                var name = $('#editCatgeoryName').val();
                var id=$('#editcategoryid').val();
                $.post("{{route('category.edit')}}",{'id':id,'name':name,'_token':$('input[name=_token]').val()},    function(data){
                    swal({
                        title: "Success",
                        text: "The Category was updated",
                        type: "success",

                        showCancelButton: true,

                    }).then(function(){
                    
                        location.reload();
                    })
                        
                })
                .fail(function(response) {
                    alert('Error: The given data is Invalid');
                });
            });
        });

        

        $(document).on('click',"[id*='deletecat']",function(event){
            var id = $(this).attr("id").slice(9);
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this file!",
                type: "warning",
        
                showCancelButton: true,
        
            }).then(function(){
                $.post("{{route('category.delete')}}",{id:id,_token:"{{csrf_token()}}"},function(data){
                    swal({
                        title:"Deleted Successfully",
                        type:"success"
            
                    }).then(function(){
                        location.reload();
                    })
                })
            });
        });

        $(document).on('click',"[id*='addSubCategoryButton']",function(event){
            var cat_id = $(this).attr("id").slice(20);
            $('#category_id_sub').val(cat_id);
        });

        $(document).on('click','#savesubcat',function(){
            $('#subcat_store_form').submit();
        });

        $(document).on('click','#editsavesubcat',function(){
            $('#subcat_edit_form').submit();
        });

        $(document).on('click',"[id*='edit_sub_category']",function(event){
            var subcat_id = $(this).attr("id").slice(17);
            var cat_id=$('#subcat_cat_id'+subcat_id).val();
            var name=$('#sub_cat_name'+subcat_id).html();
            var image=$('#sub_cat_image'+subcat_id).attr("src");
            $('#editSubCatgeoryName').val(name);
            $('#category_id_sub').val(cat_id);
            $('#my_subcat_id').val(subcat_id);
            $('#select_category'+cat_id).attr("selected","selected");
        });

        $(document).on('click',"[id*='delete_subcat']",function(event){
            var id = $(this).attr("id").slice(13);
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this file!",
                type: "warning",
        
                showCancelButton: true,
        
            }).then(function(){
                $.post("{{route('subcategory.delete')}}",{id:id,_token:"{{csrf_token()}}"},function(data){
                swal({
                    title:"Deleted Successfully",
                    type:"success"
        
                }).then(function(){
                location.reload();
                })
                })
            });
        });

        $(window).load(function(){
        $("[id*='addSubCategoryButton']").removeAttr('disabled');
        });
    </script>
@endsection